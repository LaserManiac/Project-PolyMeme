#include "Camera.h"

kr::Camera::Camera(float _wpWidth, float _wpHeight, float _zNear, float _zFar)
	: m_viewportWidth(_wpWidth), m_viewportHeight(_wpHeight), m_near(_zNear), m_far(_zFar) {}

float kr::Camera::getViewportWidth() const {
	return m_viewportWidth;
}

void kr::Camera::setViewportWidth(float _wpWidth) {
	m_viewportWidth = _wpWidth;
}

float kr::Camera::getViewportHeight() const {
	return m_viewportHeight;
}

void kr::Camera::setViewportHeight(float _wpHeight) {
	m_viewportHeight = _wpHeight;
}

float kr::Camera::getNear() const {
	return m_near;
}

void kr::Camera::setNear(float _near) {
	m_near = _near;
}

float kr::Camera::getFar() const {
	return m_far;
}

void kr::Camera::setFar(float _far) {
	m_far = _far;
}

const gmtl::Point3f& kr::Camera::getPosition() const {
	return m_position;
}

void kr::Camera::setPosition(gmtl::Point3f &_position) {
	m_position = _position;
}

const gmtl::Vec3f& kr::Camera::getDirection() const {
	return m_direction;
}

void kr::Camera::setDirection(gmtl::Vec3f &_direction) {
	m_direction = _direction;
}

const gmtl::Vec3f& kr::Camera::getUp() const {
	return m_up;
}

void kr::Camera::setUp(gmtl::Vec3f &_up) {
	m_up = _up;
}

const gmtl::Frustumf& kr::Camera::getFrustum() const {
	return m_frustum;
}

const kr::Transform3D& kr::Camera::getView() const {
	return m_view;
}

const kr::Projection& kr::Camera::getProjection() const {
	return m_projection;
}

const gmtl::Matrix44f& kr::Camera::getCombined() const {
	return m_combined;
}

void kr::Camera::rotateX(float _deg) {
	kr::Transform3D transform = kr::Transform3D::RotationX(_deg);
	m_direction = transform * m_direction;
	m_up = transform * m_up;
}

void kr::Camera::rotateY(float _deg) {
	kr::Transform3D transform = kr::Transform3D::RotationY(_deg);
	m_direction = transform * m_direction;
	m_up = transform * m_up;
}

void kr::Camera::rotateZ(float _deg) {
	kr::Transform3D transform = kr::Transform3D::RotationZ(_deg);
	m_direction = transform * m_direction;
	m_up = transform * m_up;
}

void kr::Camera::rotate(const gmtl::Vec3f &_axis, float _deg) {
	kr::Transform3D transform = kr::Transform3D::Rotation(_axis, _deg);
	m_direction = transform * m_direction;
	m_up = transform * m_up;
}

void kr::Camera::lookAt(const gmtl::Point3f &_origin, const gmtl::Point3f &_target, const gmtl::Vec3f &_up) {
	m_position = _origin;
	m_direction = _target - _origin;
	gmtl::normalize(m_direction);
	m_up = _up;
}
