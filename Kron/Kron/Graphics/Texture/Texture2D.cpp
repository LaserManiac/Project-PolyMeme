#include "Texture2D.h"

GLsizei kr::Texture2D::getWidth() const {
	return m_width;
}

GLsizei kr::Texture2D::getHeight() const {
	return m_height;
}

void kr::Texture2D::loadData(GLint _level, GLenum _format, GLenum _type, const GLvoid * _data) {
	glTexImage2D(GL_TEXTURE_2D, _level, getFormat(), m_width, m_height, 0, _format, _type, _data);
}

/*
eng::Texture2D* eng::Texture2D::GenericTexture2DLoader::load(const eng::FilePath &_path) {
	sf::Image texData;
	texData.loadFromFile(_path.getFull());
	
	eng::Texture2D* texture = new eng::Texture2D(texData.getSize().x, texData.getSize().y, GL_RGB);
	texture->bind();
	texture->setFiltering(GL_LINEAR, GL_LINEAR);
	texture->setWrapping(GL_REPEAT, GL_REPEAT, GL_REPEAT);
	texture->loadData(0, GL_RGBA, GL_UNSIGNED_BYTE, texData.getPixelsPtr());
	
	return texture;
}
*/
