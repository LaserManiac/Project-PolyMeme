#include "OglVertexArray.h"

namespace kr {

	VertexArray::VertexArray() {
		kr_glCall(glGenVertexArrays(1, &m_handle));
	}

	VertexArray::~VertexArray() {
		if (isValid())
			kr_glCall(glDeleteVertexArrays(1, &m_handle));
	}

	void VertexArray::bind() const {
		kr_glCall(glBindVertexArray(m_handle));
	}

	void VertexArray::unbind() const {
		kr_glCall(glBindVertexArray(GL_NONE));
	}

}
