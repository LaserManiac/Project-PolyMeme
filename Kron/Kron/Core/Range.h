#pragma once

#include <GMTL\Math.h>

#include "Utility\Types.h"
#include "Utility\Math.h"

namespace kr {
	
	// ====================================================
	//  An iterator class used to iterate through a range.
	// ====================================================
	template<class _Type>
	class RangeIterator {
	private:
		const _Type m_from;
		const _Type m_step;
		kr::i32 m_counter;

	public:
		RangeIterator(_Type _start, _Type _step, kr::i32 _counter = 0)
			: m_from(_start), m_step(_step), m_counter(_counter) {}

		// Go to next value in range
		RangeIterator& operator++() {
			m_counter++;
			return *this;
		}

		// Checks if this iterator is past the other one
		bool operator!=(const RangeIterator &_other) {
			return m_counter <= _other.m_counter;
		}

		// Get current iterator value
		_Type operator*() {
			return m_from + m_step * m_counter;
		}

	};



	// ==============================
	//  A range of numerical values.
	// ==============================
	template<class _Type>
	class Range {
	public:
		using Iterator = typename RangeIterator<_Type>;

	private:
		const _Type m_from;
		const _Type m_step;
		const kr::i32 m_numberCount;

	public:
		// Create a range.
		// _from : The first element in range.
		// _to : The farthest possible element in range (Not necessarily the last one!).
		// _step : The distance between elements.
		Range(_Type _from, _Type _to, _Type _step = 1)
			: m_from(_from), m_step(_step * kr::math::sign(_to - _from)), m_numberCount((kr::i32)(kr::math::abs(_to - _from) / _step)) {}

		// Gets an iterator for the first number in the range.
		Iterator begin() {
			return Iterator(m_from, m_step);
		}

		// Gets an iterator for the last number in the range.
		Iterator end() {
			return Iterator(m_from, m_step, m_numberCount);
		}

		// Returns a reverse range.
		Range operator-() {
			return Range(*end(), m_from, kr::math::abs(m_step));
		}

	};



	// A range of int32 types
	using IntRange = typename Range<kr::i32>;

	// A range of float32 types
	using FloatRange = typename Range<kr::f32>;

}