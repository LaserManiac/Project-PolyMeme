#pragma once

#include <GMTL/Matrix.h>
#include <GMTL/MatrixOps.h>
#include <GMTL/Math.h>

namespace kr {

	// ====================================================
	//  A projection class containing a projection matrix.
	//  Supports perspective and orthographic projection.
	// ====================================================
	class Projection : public gmtl::Matrix44f {
	public:
		Projection() : gmtl::Matrix44f() {}
		Projection(gmtl::Matrix44f &_mat) : gmtl::Matrix44f(_mat) {}

		Projection& setToPerspective(float _width, float _height, float _fovX, float _near, float _far);
		Projection& setToOrthographic(float _width, float _height, float _near, float _far);

		static Projection Perspective(float _width, float _height, float _fovX, float _near, float _far);
		static Projection Orthographic(float _width, float _height, float _near, float _far);

	};

}