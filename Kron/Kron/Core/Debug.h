#pragma once

/*
// Define a debug boolean flag
*/
#ifdef KRON_DEBUG
#	define kr_isDebugMode true
#else
#	define kr_isDebugMode false
#endif

/*
// A debug macro, code inside is only executed when in debug mode
*/
#if kr_isDebugMode == true
#	define kr_debug(x) x
#else
#	define kr_debug(x)
#endif

/*
// A no debug macro, code inside is only executed when not in debug mode
*/
#if kr_isDebugMode == false
#	define kr_noDebug(x) x
#else
#	define kr_noDebug(x)
#endif
