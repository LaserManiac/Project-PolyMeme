#include <iostream>
#include <vector>
#include <memory>
#include <cmath>
#include <functional>
#include <thread>
#include <condition_variable>
#include <filesystem>

#include "Kron.h"
#include "Core\Application.h"
#include "Core\Logging.h"
#include "Input\Keys.h"
#include "Input\InputListener.h"
#include "Graphics\Graphics.h"
#include "Graphics\Ogl\OglVertexArray.h"
#include "Graphics\Ogl\OglShader.h"
#include "Graphics\VertexLayout.h"
#include "Graphics\ShapeRenderer.h"
#include "Graphics\Camera\OrthographicCamera.h"



class TestApp : public kr::Application {
private:
	kr::Bool m_tickTockFlag;
	kr::VertexArray* m_pVao;
	kr::InputMultiplexer* m_pMultiplexer;
	kr::OglShaderProgram* m_pShader = nullptr;
	kr::ShapeRenderer* m_pShapeRenderer = nullptr;

public:
	TestApp(const kr::AppSettings &_settings)
		: kr::Application(_settings) {}

	void init() override {
		m_pMultiplexer = new kr::InputMultiplexer();
		setInputListener(m_pMultiplexer);

		m_pMultiplexer->addListener(new kr::ComboListener(kr::Keycode::Escape, [&](auto _isPressed) { if (_isPressed) close(); return true; }));
		m_pMultiplexer->addListener(new kr::ComboListener(kr::Keycode::Return, [&](auto _isPressed) { if (_isPressed) setFullscreenMode(!getFullscreenMode()); return true; }));
		m_pMultiplexer->addListener(new kr::ComboListener(kr::Keycode::Space, [&](auto _isPressed) { if (_isPressed) setWindowSize({ 640, 480 }); return true; }));

		kr::Logging::consoleLine("Init (GL", getContextSettings().majorVersion, getContextSettings().minorVersion, ")");

		m_pShader = kr::OglShaderProgram::loadFromKSS("res/shape_renderer.kss");
		if (!m_pShader->isLinked())
			kr::Logging::consoleLine(m_pShader->getLinkLog());

		m_pVao = new kr::VertexArray();
		m_pVao->bind();
		m_pShapeRenderer = new kr::ShapeRenderer();
		m_pShapeRenderer->setShaderProgram(m_pShader);
		kr::Logging::consoleLine("Window size: ", getWindowSize());
		kr::OrthographicCamera camera;
		camera.setToOrtho2D(0, 0, (float)getWindowSize()[0], (float)getWindowSize()[1], true);
		m_pShapeRenderer->setViewProjectionMatrix(camera.getCombined());
		kr_glCall(glViewport(0, 0, getWindowSize()[0], getWindowSize()[1]));



		try {
			kr::VertexLayout* layout = kr::VertexLayout::loadFromJVL("res/default_layout.jvl");
			std::cout << *layout << std::endl;
			delete layout;
		} catch (const kr::Exception &e) {
			std::cout << e.getMessage() << std::endl;
		}
	}

	void tick() override {

	}

	void update() override {

	}

	void render() override {
		kr_glCall(glClearColor(1, 1, 1, 1));
		kr_glCall(glClear(GL_COLOR_BUFFER_BIT));
		kr_glCall(glDisable(GL_CULL_FACE));

		float w = (kr::f32)getWindowSize()[0];
		float h = (kr::f32)getWindowSize()[1];
		float b = 2;
		
		m_pVao->bind();
		m_pShapeRenderer->begin(kr::ShapeRenderer::FILLED);
		
		kr::i32 n = 32;
		kr::f32 step = 2 * gmtl::Math::PI / n;
		kr::f32 radius = gmtl::Math::Min(getWindowSize()[0], getWindowSize()[1]) * 0.25f;
		kr::f32 cx = (float)getWindowSize()[0] / 2;
		kr::f32 cy = (float)getWindowSize()[1] / 2;
		kr::f32 lpx, lpy;
		for (kr::i32 i = 0; i <= n; i++) {
			kr::f32 angle = step * i;
			kr::f32 px = cx + gmtl::Math::sin(angle) * radius;
			kr::f32 py = cy + gmtl::Math::cos(angle) * radius;

			if (i != 0) {
				m_pShapeRenderer->setColor({ 1, 0, 0 });
				m_pShapeRenderer->vertex({ cx, cy, 0 });
				m_pShapeRenderer->vertex({ lpx, lpy, 0 });
				m_pShapeRenderer->vertex({ px, py, 0 });
			}

			lpx = px;
			lpy = py;
		}

		m_pShapeRenderer->end();
	}

	void end() override {
		if (m_pShapeRenderer != nullptr)
			delete m_pShapeRenderer;
		if (m_pShader != nullptr)
			delete m_pShader;
		kr::Logging::consoleLine("End");
	}

	void resize() override {
		kr::OrthographicCamera camera;
		camera.setToOrtho2D(0, 0, (float)getWindowSize()[0], (float)getWindowSize()[1], true);
		m_pShapeRenderer->setViewProjectionMatrix(camera.getCombined());
		kr_glCall(glViewport(0, 0, getWindowSize()[0], getWindowSize()[1]));

		kr::Logging::consoleLine("Resize to ", getWindowSize()[0], "x", getWindowSize()[1]);
	}

	void pause() override {
		kr::Logging::consoleLine("Pause");
	}

	void resume() override {
		kr::Logging::consoleLine("Resume");
	}

	void recover() override {
		delete m_pVao;
		m_pVao = new kr::VertexArray();
		m_pVao->bind();
	}

};

int main() {

	kr::AppSettings settings;
	settings.tickRate = 30;
	settings.windowTitle = "Project PolyMeme";
	settings.windowSize = { 640, 480 };
	settings.windowFullscreen = false;
	settings.contextSettings.attributeFlags = sf::ContextSettings::Attribute::Core;
	settings.contextSettings.majorVersion = 3;
	settings.contextSettings.minorVersion = 3;
	kr::Application::start<TestApp>(settings);

	return EXIT_SUCCESS;
}
