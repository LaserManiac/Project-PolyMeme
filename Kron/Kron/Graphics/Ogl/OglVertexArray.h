#pragma once

#include <unordered_map>

#include "Graphics\Graphics.h"
#include "Graphics\Ogl\OglResource.h"
#include "Graphics\Ogl\OglBuffer.h"

namespace kr {

	// =========================
	//  An OpenGL vertex array.
	// =========================
	class VertexArray : public kr::GLResource {
	private:
		mutable GLuint m_attributeIndexOffset = 0;

	public:
		VertexArray();
		~VertexArray();

		// Bind vertex array
		void bind() const;

		// Unbind vertex array
		void unbind() const;

	};

}