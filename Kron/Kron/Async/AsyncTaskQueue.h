#pragma once

#include <queue>
#include <list>
#include <functional>
#include <mutex>
#include <condition_variable>

#include "Obsolete\Task.h"
#include "Core\ObjectProvider.h"

namespace kr {
	
	/*
	// A thread safe implementation of ObjectProvider<Task> through a queue of Task pointers.
	*/
	class AsyncTaskQueue : public ObjectProvider<Task> {
	private:
		std::mutex m_mutex;
		std::condition_variable m_conditionVariable;
		std::queue<Task*, std::list<Task*>> m_tasks;
		bool m_closed = false;
		int m_waitingThreadCount = 0;

	public:
		// Adds a task to the queue.
		// If there are threads waiting, one thread is notified.
		// If the queue is closed, an exception is thrown.
		void enqueue(Task* _pTask);

		// Returns the next task in the queue.
		// If there are no tasks in the queue, the caller thread waits until notified.
		// If the queue is closed, remaining tasks are returned one by one, and NULL if there are no tasks left.
		Task* obtain() override;

		// Deletes a task.
		void release(Task* _pTask) override;

		// Closes the queue.
		void close();
	};

}