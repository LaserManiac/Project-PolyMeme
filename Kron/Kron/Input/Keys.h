#pragma once

#include <SFML\Window.hpp>

#include "Core\Types.h"

namespace kr {

	// ===============================
	//  A list of keys on the keyboard.
	// ===============================
	using Keycode = typename sf::Keyboard::Key;

	// ============================================================================================
	//  A key combination which includes a single key and optionally the ctrl/alt/shift modifiers.
	// ============================================================================================
	class Combo {
	public:
		// Key modifiers.
		enum Modifier {
			NONE  = 0,
			CTRL  = 1,
			ALT   = 2,
			SHIFT = 4
		};

		// A list of key modifiers.
		using Modifiers = kr::u8;

	private:
		Keycode m_keycode;
		Modifiers m_modifiers;

	public:
		// Constructor
		// _code : Keycode of the main key.
		// _modifiers : Modifier keys that must be pressed (ctrl/alt/shift).
		Combo(Keycode _code, Modifiers _modifiers = Modifier::NONE)
			: m_keycode(_code), m_modifiers(_modifiers) {}

		Keycode getKeycode() { return m_keycode; }
		kr::Bool hasModifiers() { return m_modifiers != Modifier::NONE; }
		kr::Bool hasCtrl() { return m_modifiers & Modifier::CTRL; }
		kr::Bool hasAlt() { return m_modifiers & Modifier::ALT; }
		kr::Bool hasShift() { return m_modifiers & Modifier::SHIFT; }

	};

}
