#include "Projection.h"

kr::Projection& kr::Projection::setToPerspective(float _width, float _height, float _fovYdeg, float _near, float _far) {
	gmtl::zero(*this);
	mState = XformState::FULL;
	gmtl::Matrix44f& self = *this;

	float tanFovY = gmtl::Math::tan(gmtl::Math::deg2Rad(_fovYdeg / 2.0f));
	float tanFovX = static_cast<float>(tanFovY * _width / _height);
	float depth = _far - _near;

	self(0, 0) = 1.0f / tanFovX;
	self(1, 1) = 1.0f / tanFovY;
	self(2, 2) = -(_far + _near) / depth;
	self(2, 3) = -2.0f * _far * _near / depth;
	self(3, 2) = -1.0f;

	return *this;
}

kr::Projection & kr::Projection::setToOrthographic(float _width, float _height, float _near, float _far) {
	gmtl::zero(*this);
	mState = XformState::FULL;
	gmtl::Matrix44f& self = *this;

	float depth = _far - _near;

	self(0, 0) = 2.0f / _width;
	self(1, 1) = 2.0f / _height;
	self(2, 2) = -2.0f / depth;
	self(2, 3) = -(_far + _near) / depth;
	self(3, 3) = 1.0f;

	return *this;
}

kr::Projection kr::Projection::Perspective(float _width, float _height, float _fovX, float _near, float _far) {
	return Projection().setToPerspective(_width, _height, _fovX, _near, _far);
}

kr::Projection kr::Projection::Orthographic(float _width, float _height, float _near, float _far) {
	return Projection().setToOrthographic(_width, _height, _near, _far);
}
