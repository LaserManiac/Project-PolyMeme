#include "Application.h"

#include <SFML\System\Time.hpp>

#include "Graphics\Graphics.h"
#include "Logging.h"
#include "Exception.h"

namespace kr {
	
	Application::Application(const AppSettings &_settings)
		:	m_tickRate(_settings.tickRate),
			m_title(_settings.windowTitle),
			m_fullscreen(_settings.windowFullscreen),
			m_vSync(_settings.vSync),
			m_windowSize(_settings.windowSize) {

		if (m_fullscreen) {
			sf::VideoMode videoMode = sf::VideoMode::getFullscreenModes().front();
			m_window.create(videoMode, m_title, sf::Style::Fullscreen, _settings.contextSettings);
			m_windowSize[0] = m_window.getSize().x;
			m_windowSize[1] = m_window.getSize().y;
		} else {
			sf::VideoMode videoMode(m_windowSize[0], m_windowSize[1], 32U);
			m_window.create(videoMode, m_title, sf::Style::Default, _settings.contextSettings);
		}
		m_window.setVerticalSyncEnabled(m_vSync);
		m_window.setKeyRepeatEnabled(false);
	}

	Application::~Application() {
		if (m_pInputListener != NULL)
			delete m_pInputListener;
	}

	kr::InputListener* Application::setInputListener(kr::InputListener* _pInputListener) {
		InputListener* pOldListener = m_pInputListener;
		m_pInputListener = _pInputListener;
		return pOldListener;
	}

	void Application::setFullscreenMode(kr::Bool _fullscreen) {
		if (m_fullscreen == _fullscreen)
			return;

		m_fullscreen = _fullscreen;

		sf::VideoMode videoMode;
		kr::Enum style;
		if (m_fullscreen) {
			videoMode = sf::VideoMode::getFullscreenModes().front();
			style = sf::Style::Fullscreen;
		} else {
			videoMode = sf::VideoMode(m_windowSize[0], m_windowSize[1]);
			style = sf::Style::Default;
		}

		sf::ContextSettings contextSettings = getContextSettings();
		m_window.create(videoMode, m_title, style, contextSettings);
		m_window.setActive(true);
		m_window.setVerticalSyncEnabled(m_vSync);
		m_window.setKeyRepeatEnabled(false);

		gmtl::Vec2u prevSize = getWindowSize();
		m_windowSize[0] = m_window.getSize().x;
		m_windowSize[1] = m_window.getSize().y;

		recover();

		gmtl::Vec2u newSize = getWindowSize();
		if (prevSize != newSize)
			resize();
	}

	void Application::setTitle(const kr::String _title) {
		m_title = _title;
		m_window.setTitle(m_title);
	}

	void Application::setVSyncEnabled(kr::Bool _enabled) {
		m_vSync = _enabled;
		m_window.setVerticalSyncEnabled(m_vSync);
	}

	void Application::setWindowSize(const gmtl::Vec2u &_size) {
		if(m_fullscreen)
			setFullscreenMode(false);
		m_windowSize = _size;
		m_window.setSize({ m_windowSize[0], m_windowSize[1] });
	}

	void Application::run() {
		// Activate OpenGL context on current thread.
		m_window.setActive(true);

		// Initialize Kron graphics
		if (kr::Graphics::init())
			kr::Logging::consoleLine("ERROR: Failed to initialize Kron graphics!");

		// Initialize application
		init();

		// Application loop
		sf::Clock clock;
		sf::Time accumulator;
		clock.restart();
		while (!m_closeRequested) {

			// Process window events
			processEvents();
			
			// Tick application
			sf::Time targt = sf::microseconds(1000000 / m_tickRate);
			sf::Time delta = clock.restart();
			accumulator += delta;
			while (accumulator >= targt && !m_closeRequested) {
				accumulator -= targt;
				tick();
			}

			// Update application
			update();

			// Render application
			render();

			// Swap buffers
			m_window.display();
		}

		// End application
		end();

		// Close window
		m_window.close();
	}

	void Application::close() {
		m_closeRequested = true;
	}

	void Application::processEvents() {
		kr::Bool closeRequested = false;

		sf::Event event;
		while (m_window.pollEvent(event)) {
			switch (event.type) {
				case sf::Event::EventType::Closed: {
					close();
					break;
				}

				case sf::Event::EventType::Resized: {
					sf::Vector2u size = m_window.getSize();
					m_windowSize = { size.x, size.y };
					resize();
					break;
				}

				case sf::Event::EventType::LostFocus: {
					pause();
					break;
				}

				case sf::Event::EventType::GainedFocus: {
					resume();
					break;
				}

				case sf::Event::EventType::KeyPressed: {
					if (m_pInputListener != NULL)
						m_pInputListener->keyPressed(event.key.code, event.key.control, event.key.alt, event.key.shift);
					break;
				}

				case sf::Event::EventType::KeyReleased: {
					if (m_pInputListener != NULL)
						m_pInputListener->keyReleased(event.key.code, event.key.control, event.key.alt, event.key.shift);
					break;
				}

				case sf::Event::EventType::MouseButtonPressed: {
					if (m_pInputListener != NULL)
						m_pInputListener->mouseButtonPressed(event.mouseButton.button, event.mouseButton.x, event.mouseButton.y);
					break;
				}

				case sf::Event::EventType::MouseButtonReleased: {
					if (m_pInputListener != NULL)
						m_pInputListener->mouseButtonReleased(event.mouseButton.button, event.mouseButton.x, event.mouseButton.y);
					break;
				}

				case sf::Event::EventType::MouseWheelScrolled: {
					if (m_pInputListener != NULL)
						m_pInputListener->mouseWheelScrolled(event.mouseWheel.delta);
					break;
				}

				case sf::Event::EventType::MouseMoved: {
					if (m_pInputListener != NULL)
						m_pInputListener->mouseMoved(event.mouseMove.x, event.mouseMove.y);
					break;
				}

				case sf::Event::EventType::TextEntered: {
					if (m_pInputListener != NULL)
						m_pInputListener->textTyped(event.text.unicode);
					break;
				}
			}
		}
	}

	
	
	Application* Application::s_currentApp = nullptr;

	Application& Application::getCurrent() {
		if (s_currentApp == nullptr)
			throw kr::InvalidStateException("There is currently no running Kron application.");
		return *s_currentApp;
	}

}
