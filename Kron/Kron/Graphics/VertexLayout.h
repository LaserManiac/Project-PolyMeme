#pragma once

#include <vector>
#include <unordered_map>
#include <iostream>

#include "Core\Types.h"
#include "Core\String.h"
#include "Core\Files.h"
#include "Core\Json.h"
#include "Graphics\Graphics.h"

namespace kr {

	// ==========================================
	//  An attribute in an OpenGL vertex layout.
	// ==========================================
	class VertexAttribute {
	public:
		// A type of element in a vertex attribute.
		enum Type : kr::Enum {
			FLOAT,	// 32bit floating point number
			INT,	// 32bit integer
			UINT,	// 32bit unsigned integer
			SHORT,	// 16bit integer
			USHORT,	// 16bit unsigned integer
			BYTE,	// 8bit integer
			UBYTE	// 8bit unsigned integer
		};
		
		// Get size of an element type in bytes.
		static kr::u8 getTypeBytes(Type _type);

		// Usage of a vertex attribute.
		enum Usage : kr::Enum {
			STATIC,	// Uploaded once and infrequently or never changed.
			DYNAMIC // Reuploaded or changed frequently.
		};

	private:
		const Type m_type;				// Type of elements in the attribute.
		const Usage m_usage;			// Usage of the attribute
		const kr::u8 m_size;			// Number of elements in the attribute. 
		const kr::Bool m_normalized;	// Whether the attribute should be normalized when accessed.
		const kr::String m_name;		// Name of the attribute.

	public:
		// _type:		Type of element in the attribute.
		// _size:		Number of elements in the attribute - 1, 2, 3, or 4.
		// _normalized:	Whether the attribute data should be normalized (converted from fixed to floating point) when accessed.
		VertexAttribute(Type _type, kr::u8 _size, kr::Bool _normalized, Usage _usage, const kr::String &_name)
			: m_type(_type),
			m_size(_size),
			m_normalized(_normalized),
			m_usage(_usage),
			m_name(_name) {}

		// Get type of elements in attribute.
		inline Type getElementType() const { return m_type; }

		// Get number of elements in attribute.
		inline kr::u8 getElementNumber() const { return m_size; }

		// Get usage of attribute.
		inline Usage getUsage() const { return m_usage; }

		// Check if attribute should be normalized.
		inline kr::Bool isNormalized() const { return m_normalized; }

		// Get size of attribute in bytes.
		inline kr::u8 getByteSize() const { return getTypeBytes(m_type) * m_size; }

		// Get name of attribute.
		inline const kr::String& getName() const { return m_name; }

	};



	// ==========================================================================
	//  A series of vertex attribute layouts comprising an OpenGL vertex buffer.
	// ==========================================================================
	class VertexLayout {
	private:
		kr::u16 m_byteSize;							// Total size of vertex in bytes.
		std::vector<VertexAttribute> m_attributes;	// A list of vertex attributes.

	public:
		// _attributes:	Vector of vertex attributes.
		VertexLayout(const std::vector<VertexAttribute> &_attributes);

		// Get size of a single vertex in this buffer, in bytes.
		inline GLsizei getByteSize() const { return m_byteSize; };

		// Get list of attribute layouts.
		inline const std::vector<VertexAttribute>& getAttributes() const { return m_attributes; }

	public:
		// Load a vertex layout from a Json Vertex Layout file.
		static VertexLayout* loadFromJVL(const kr::FilePath &_path);
		static VertexLayout* createFromJVL(const kr::Json &_json);

	};

	std::ostream& operator<<(std::ostream &_out, const VertexLayout &_layout);

}
