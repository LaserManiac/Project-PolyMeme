#pragma once

#include <math.h>

#include <SFML\System.hpp>

namespace kr {

	// A generic byte type
	using Byte = typename std::int8_t;

	// A boolean type
	using Bool = typename bool;

	// A default uint enumerator type
	using Enum = typename uint32_t;

	// A character type
	using Char = typename char;

	// Integer types
	using i8 = typename std::int8_t;
	using i16 = typename std::int16_t;
	using i32 = typename std::int32_t;
	using i64 = typename std::int64_t;

	// Unsigned integer types
	using u8 = typename std::uint8_t;
	using u16 = typename std::uint16_t;
	using u32 = typename std::uint32_t;
	using u64 = typename std::uint64_t;

	// Floating point types
	using f32 = typename std::float_t;
	using f64 = typename std::double_t;

}
