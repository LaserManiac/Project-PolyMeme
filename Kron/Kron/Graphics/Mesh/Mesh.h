#pragma once

#include <iostream>
#include <vector>

#include "..\Graphics.h"
#include "..\..\Types.h"
#include "..\Buffer.h"
#include "VertexArray.h"
#include "MeshData.h"

namespace kr {
	namespace gfx {

		/*
		// An openGL mesh consisting of an element buffer containing index data,
		// one or more array buffers containing vertex data,
		// and a vertex array object used for binding the mesh.
		// The mesh object keeps a copy of its layout.
		*/
		class Mesh : public sf::NonCopyable {
		private:
			MeshData* m_pData;
			GLenum m_primitiveType;

			VertexArray m_vertexArray;
			ElementBuffer m_indexBuffer;
			std::vector<ArrayBuffer> m_arrayBuffers;

		public:
			Mesh(MeshData* _pData);

			const MeshData* getMeshData() const;
			const VertexArray& getVerteyArray() const;
			const ElementBuffer& getIndexBuffer() const;
			const std::vector<ArrayBuffer>& getArrayBuffers() const;

		public:
			static void draw(const Mesh &_mesh);

		};

	}
}