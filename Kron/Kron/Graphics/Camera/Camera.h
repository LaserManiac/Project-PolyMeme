#pragma once

#include "Core\Math.h"
#include "Graphics\Projection.h"
#include "Graphics\Transform.h"

namespace kr {

	class Camera {

	protected:
		float m_viewportWidth;
		float m_viewportHeight;
		float m_near;
		float m_far;
		gmtl::Point3f m_position;
		gmtl::Vec3f m_direction;
		gmtl::Vec3f m_up;
		gmtl::Frustumf m_frustum;
		kr::Transform3D m_view;
		kr::Projection m_projection;
		gmtl::Matrix44f m_combined;

	public:
		Camera() {};
		Camera(float _wpWidth, float _wpHeight, float _zNear, float _zFar);

		float getViewportWidth() const;
		void setViewportWidth(float _wpWidth);

		float getViewportHeight() const;
		void setViewportHeight(float _wpHeight);

		float getNear() const;
		void setNear(float _near);

		float getFar() const;
		void setFar(float _far);

		const gmtl::Point3f& getPosition() const;
		void setPosition(gmtl::Point3f &_position);

		const gmtl::Vec3f& getDirection() const;
		void setDirection(gmtl::Vec3f &_direction);

		const gmtl::Vec3f& getUp() const;
		void setUp(gmtl::Vec3f &_up);
		
		const gmtl::Frustumf& getFrustum() const;
		const kr::Transform3D& getView() const;
		const kr::Projection& getProjection() const;
		const gmtl::Matrix44f& getCombined() const;

		void rotateX(float _deg);
		void rotateY(float _deg);
		void rotateZ(float _deg);
		void rotate(const gmtl::Vec3f &_axis, float _deg);

		void lookAt(const gmtl::Point3f &_origin, const gmtl::Point3f &_target, const gmtl::Vec3f &_up);

		virtual void update() = 0;

	};

}
