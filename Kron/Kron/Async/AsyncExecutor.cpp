#include "AsyncExecutor.h"

namespace kr {

	AsyncExecutor::AsyncExecutor(int _threadCount) {
		while (_threadCount--)
			m_workerThreads.emplace_back(&m_taskQueue);
	}

	AsyncExecutor::~AsyncExecutor() {
		m_taskQueue.close();
		waitForTasks();
		m_workerThreads.clear();
	}

	int AsyncExecutor::getThreadCount() const {
		return m_workerThreads.size();
	}

	int AsyncExecutor::getTaskCount() {
		return m_semaphore.counter();
	}

	void AsyncExecutor::waitForTasks() {
		m_semaphore.wait();
	}

}