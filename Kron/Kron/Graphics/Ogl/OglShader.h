#pragma once

#include <vector>
#include <map>

#include "Core\Math.h"
#include "Core\Class.h"
#include "Core\Files.h"
#include "Graphics\Graphics.h"
#include "Graphics\Ogl\OglResource.h"

namespace kr {

	// ===============================================================
	//  An OpenGL shader stage which needs to be linked to a program. 
	// ===============================================================
	class OglShaderStage : public kr::GLResource {
	private:
		GLuint m_stage;
		kr::Bool m_compiled;
		kr::String m_compileLog;

	public:
		// Compiles the shader stage and retrieves the compile log if compilation was unsuccessful.
		// _stage : Shader stage of the shader, eg. GL_VERTEX_SHADER, GL_FRAGMENT_SHADER, GL_GEOMETRY_SHADER, etc.
		// _source : A string containg the source code for the shader.
		OglShaderStage(GLenum _stage, const kr::String &_source);
		~OglShaderStage();

		// Check if the shader stage was successfuly compiled.
		kr::Bool isCompiled() const;

		// Get the shader stage compile log.
		const kr::String& getCompileLog() const;
	};



	// ===========================
	//  An OpenGL shader program.
	// ===========================
	class OglShaderProgram : public kr::GLResource {
	public:
		// A shader uniform.
		class Uniform {
		public:
			const GLint location;
			const GLenum type;

			Uniform(GLint _location, GLenum _type)
				: location(_location), type(_type) {}
		};

		// A shader attribute.
		class Attribute {
		public:
			const GLint location;
			const GLenum type;

			Attribute(GLint _location, GLenum _type)
				: location(_location), type(_type) {}
		};

	private:
		kr::Bool m_linked;
		kr::String m_linkLog;
		std::unordered_map<kr::String, Uniform> m_uniformCache;
		std::unordered_map<kr::String, Attribute> m_attributeCache;

	public:
		// Links all provided shader stages to this shader program.
		// _stages : A vector of shader stages.
		OglShaderProgram(const std::vector<OglShaderStage*> &_stages);
		~OglShaderProgram();

		// Check if the shader program was successfuly linked.
		kr::Bool isLinked() const { return m_linked; };

		// Get the shader program link log.
		const kr::String& getLinkLog() const { return m_linkLog; };

		// Get a map of all active attributes in this shader program.
		const std::unordered_map<kr::String, Attribute> getAttributes() const { return m_attributeCache; };
		// Get the location of an active attribute in this shader program.
		GLint getAttributeLocation(const kr::String &_name) const;

		// Get a map of all active uniforms in this shader program.
		const std::unordered_map<kr::String, Uniform> getUniforms() const { return m_uniformCache; };
		// Get the location of an active uniform in this shader program.
		GLint getUniformLocation(const kr::String &_name) const;

		// Begin using shader.
		void begin(); 
		
		// Stop using shader.
		void end(); 

		// Set shader uniform by index.
		void setUniform(kr::i32 _index, kr::i32 _value);
		void setUniform(kr::i32 _index, kr::u32 _value);
		void setUniform(kr::i32 _index, kr::f32 _value);
		void setUniform(kr::i32 _index, const gmtl::Vec2i &_value);
		void setUniform(kr::i32 _index, const gmtl::Vec2u &_value);
		void setUniform(kr::i32 _index, const gmtl::Vec2f &_value);
		void setUniform(kr::i32 _index, const gmtl::Vec3i &_value);
		void setUniform(kr::i32 _index, const gmtl::Vec3u &_value);
		void setUniform(kr::i32 _index, const gmtl::Vec3f &_value);
		void setUniform(kr::i32 _index, const gmtl::Vec4i &_value);
		void setUniform(kr::i32 _index, const gmtl::Vec4u &_value);
		void setUniform(kr::i32 _index, const gmtl::Vec4f &_value);
		void setUniform(kr::i32 _index, const gmtl::Matrix33f &_value);
		void setUniform(kr::i32 _index, const gmtl::Matrix44f &_value);

		// Set shader uniform by name.
		// Returns false if there is no uniform with the given name, true otherwise.
		template<class _Type>
		kr::Bool setUniform(const kr::String &_name, const _Type &_value) {
			kr::i32 index = getUniformLocation(_name);
			if (index == -1)
				return false;
			setUniform(index, _value);
			return true;
		}

	private:
		// Cache all active attribute data in this shader program.
		void cacheAttributes();

		// Cache all active uniform data in this shader program.
		void cacheUniforms();
		
	public:
		// Trims array parenthesis from a shader resource name.
		// Eg. "v_TexCoord[0]" is trimmed to "v_TexCoord"
		static void trimResourceName(std::vector<GLchar> &_name);

		// Loads a shader from a JShader file.
		static OglShaderProgram* loadFromKSS(const kr::FilePath &_path);

	};

}