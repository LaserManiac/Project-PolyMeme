#pragma once

#include <functional>

namespace kr{

	using Task = typename std::function<void()>;

}