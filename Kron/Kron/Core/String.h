#pragma once

#include <sstream>
#include <string>

#include "Types.h"

namespace kr {

	// Define string types
	using String = typename std::basic_string<Char>;
	using StringStream = typename std::basic_stringstream<Char>;

	// ===================================================
	//  A simple class containing string utility methods.
	// ===================================================
	class StringUtils {
	private:
		// Inputs the provided object into the provided stream
		template<class _StreamType, class _StringType>
		static void buildStringBase(_StreamType &_stream, const _StringType &_str) {
			_stream << _str;
		}

		// Inputs all of the provided objects into the provided stream
		template<class _StreamType, class _StringType, class... _OtherTypes>
		static void buildStringBase(_StreamType &_stream, const _StringType &_str, const _OtherTypes&... _others) {
			_stream << _str;
			buildStringBase(_stream, _others...);
		}

		// Returns a string created from the arguments, built using a stringstream
		template<class _ReturnType, class... _StringTypes>
		static std::basic_string<_ReturnType> buildString(const _StringTypes&... _data) {
			std::basic_stringstream<_ReturnType> stream;
			buildStringBase(stream, _data...);
			return stream.str();
		}

	public:

		// Builds a kr::String out of a list of arguments, using a kr::StringStream
		template<class... _StringTypes>
		static String build(const _StringTypes&... _data) {
			return buildString<kr::Char>(_data...);
		}

	};

	// Allow for any std::basic_string<> to be input into the Kron stringstreams
	template<class _CharType>
	StringStream& operator<<(StringStream &_stream, const std::basic_string<_CharType> &_string) {
		for (const _CharType &c : _string)
			_stream << static_cast<Char>(c);
		return _stream;
	}

	// A convenient way to build a string, takes care of string streams for you
	// and automatically formats to Kron string format.
	template<class... _StringTypes>
	kr::String str(const _StringTypes&... _data) {
		return StringUtils::build(_data...);
	}

}
