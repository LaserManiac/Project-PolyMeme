#pragma once

#include <thread>

#include <SFML\System\NonCopyable.hpp>

#include "Core\ObjectProvider.h"

namespace kr {

	/*
	// Starts a thread which obtains task pointers from an ObjectProvider and executes them.
	// If a NULL pointer is recieved, the thread shuts down.
	*/
	class WorkerThread : public sf::NonCopyable {
	private:
		std::thread m_thread;

	public:
		// Constructor that takes a task provider and starts the thread.
		WorkerThread(ObjectProvider<std::function<void()>>* _pTaskProvider);

		// Destructor that joins the thread if it is joinable.
		~WorkerThread();

	};

}