#include "GlyphLayout.h"

void kr::GlyphLayout::addGlyph(sf::Uint16 _code, const Glyph _glyph) {
	m_glyphs.emplace(_code, _glyph);
}

const kr::GlyphLayout::Glyph& kr::GlyphLayout::getGlyph(sf::Uint16 _code) const {
	return m_glyphs.at(_code);
}
