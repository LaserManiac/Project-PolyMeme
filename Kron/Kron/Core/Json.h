#pragma once

#include <fstream>

#include <JSON\json.hpp>

#include "Types.h"
#include "Exception.h"
#include "String.h"
#include "Files.h"

namespace kr {

	// ==========================================
	//  DOCS at https://github.com/nlohmann/json
	//  Currently using v3.1.1
	// ==========================================
	using Json = typename nlohmann::json;

	class JsonUtils {
	public:
		// Load json from a file.
		// _json:	Json object to load into.
		// _path:	Path to file to load.
		static void loadFromFile(Json &_json, const kr::FilePath &_path) {
			std::ifstream input(_path);
			if (input.bad())
				throw kr::FileNotFoundException(kr::StringUtils::build("File at ", _path.string(), " was not found."));
			input >> _json;
		}
	};

}