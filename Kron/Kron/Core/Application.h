#pragma once

#include <SFML\Window.hpp>

#include "Core\Types.h"
#include "Core\Math.h"
#include "Core\String.h"
#include "Input\InputListener.h"

namespace kr {

	// ==========================================================
	//  A simple struct holding settings for a Kron application.
	// ==========================================================
	struct AppSettings {

		// Number of ticks in one second.
		kr::u16 tickRate = 30U;

		// Title of the application window.
		kr::String windowTitle = "Kron Application";

		// Size of the application window.
		gmtl::Vec2u windowSize;

		// Whether to enable vertical sync.
		kr::Bool vSync;

		// Whether the application window should be in full screen mode.
		kr::Bool windowFullscreen;

		// SFML OpenGL context settings. Check SFML docs for more info.
		sf::ContextSettings contextSettings;

	};



	// ====================================================
	//  An abstract class representing a Kron application.
	//  Check AppSettings above for property definitions.
	// ====================================================
	class Application {
	private:
		kr::u16 m_tickRate;
		kr::String m_title;
		kr::Bool m_fullscreen;
		kr::Bool m_vSync;
		gmtl::Vec2u m_windowSize;

		sf::Window m_window;
		kr::Bool m_closeRequested = false;
		kr::InputListener* m_pInputListener = NULL;

	public:
		// Constructor that takes application settings.
		// _settings : An AppSettings object containing all the info needed to construct the application.
		Application(const AppSettings &_settings);

		// Destructor
		// Deletes m_pInputListener
		~Application();

		// Get OpenGL context settings of the application window.
		const sf::ContextSettings& getContextSettings() const { return m_window.getSettings(); };

		// Get ticks per second for this application.
		kr::u16 getTickRate() const { return m_tickRate; }
		// Set ticks per second for this application.
		void setTickRate(kr::u16 _tickRate) { m_tickRate = _tickRate; }

		// Get input listener for this application.
		kr::InputListener* getInputListener() { return m_pInputListener; }
		// Set input listener for this application.
		// Takes ownership of the passed object.
		// Old listener WILL NOT be deleted, instead it is returned.
		kr::InputListener* setInputListener(kr::InputListener* _pInputListener);

		// Enables or disables application window fullscreen mode.
		void setFullscreenMode(kr::Bool _fullscreen);
		// Check if the application window is in fulscreen mode.
		kr::Bool getFullscreenMode() const { return m_fullscreen; }

		// Set title of the application window.
		void setTitle(const kr::String _title);
		// Get title of the application window.
		const kr::String& getTitle() const { return m_title; };

		// Enable or disable vertical sync for the application window.
		void setVSyncEnabled(kr::Bool _enabled);
		// Check if vertical sync is enabled.
		kr::Bool getVSyncEnabled() const { return m_vSync; }

		// Set size of the application window.
		void setWindowSize(const gmtl::Vec2u &_size);
		// Get size of the application window.
		const gmtl::Vec2u& getWindowSize() const { return m_windowSize; }

		// Start the application loop on the current thread.
		void run();

		// Request for the application to close down.
		void close();

		virtual void init() = 0;
		virtual void tick() = 0;
		virtual void update() = 0;
		virtual void render() = 0;
		virtual void end() = 0;
		virtual void resize() = 0;
		virtual void pause() = 0;
		virtual void resume() = 0;
		virtual void recover() = 0;

	private:
		// Process window events and delegate them to the input listener if needed.
		void processEvents();



	private:
		static Application* s_currentApp;

	public:
		// Start an Application of type _AppClass.
		template<class _AppClass>
		static void start(const AppSettings &_settings) {
			if (s_currentApp != nullptr)
				throw kr::InvalidStateException("A Kron application is already running.");
			
			s_currentApp = new _AppClass(_settings);
			s_currentApp->run();
			
			delete s_currentApp;
			s_currentApp = nullptr;
		}

		// Get currently running application.
		static Application& getCurrent();

	};

}
