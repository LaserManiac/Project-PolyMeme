#include "Semaphore.h"

#include <iostream>

#include "Core\String.h"

namespace kr {

	Semaphore::Semaphore(int _counter)
		: m_counter(_counter) {}

	void Semaphore::obtain() {
		std::lock_guard<std::mutex> lock(m_mutex);
		m_counter++;
	}

	void Semaphore::release() {
		std::lock_guard<std::mutex> lock(m_mutex);

		if (m_counter <= 0)
			throw Exception(kr::str("Semaphore counter is already zero!"));

		m_counter--;
		if (m_counter == 0)
			m_condition.notify_all();
	}

	void Semaphore::wait() {
		std::unique_lock<std::mutex> lock(m_mutex);
		if(m_counter > 0)
			m_condition.wait(lock);
	}

	int Semaphore::counter() {
		std::unique_lock<std::mutex> lock(m_mutex);
		return m_counter;
	}

}