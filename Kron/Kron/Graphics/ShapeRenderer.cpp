#include "ShapeRenderer.h"

#include "Core\Exception.h"

namespace kr {

	void ShapeRenderer::begin(ShapeType _shapeType) {
		if (m_shapeType != ShapeType::NONE)
			throw kr::InvalidStateException("Must call end() before begin() on ShapeRenderer.");

		m_shapeType = _shapeType;
		m_data.clear();
	}

	void ShapeRenderer::vertex(const gmtl::Vec3f &_position) {
		if (m_shapeType == ShapeType::NONE)
			throw kr::InvalidStateException("Must call begin() before drawing a vertex.");

		m_data.emplace_back(_position[0]);
		m_data.emplace_back(_position[1]);
		m_data.emplace_back(_position[2]);

		m_data.emplace_back(m_color.r);
		m_data.emplace_back(m_color.g);
		m_data.emplace_back(m_color.b);
	}

	void ShapeRenderer::end() {
		if (m_shapeType == ShapeType::NONE)
			throw kr::InvalidStateException("Must call begin() before end() on ShapeRenderer.");

		flush();
	}

	void ShapeRenderer::flush() {
		// Check if the number of vertices in the buffer is ok for building the primitive.
		kr::u32 vertexNum = m_data.size() / VERTEX_SIZE;
		if (m_shapeType == ShapeType::FILLED && vertexNum % VERTS_IN_TRIANGLE != 0)
			throw kr::InvalidStateException("Number of vertices for GL_TRIANGLES must be divisible by 3.");
		else if (m_shapeType == ShapeType::LINE && vertexNum % VERTS_IN_LINE != 0)
			throw kr::InvalidStateException("Number of vertices for GL_LINES must be divisible by 2.");

		// Check if we have a shader to render with.
		if (m_pShader == nullptr)
			throw kr::InvalidStateException("There is no shader set for this ShapeRenderer.");
		
		// Upload data.
		m_arrayBuffer.bind();
		m_arrayBuffer.uploadData(m_data, GL_STREAM_DRAW);
		kr_glCall(glEnableVertexAttribArray(0));
		kr_glCall(glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * VERTEX_SIZE, nullptr));
		kr_glCall(glEnableVertexAttribArray(1));
		kr_glCall(glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * VERTEX_SIZE, (void*)(sizeof(GLfloat) * 3))); // Offset is 3 because Vec3 position comes first in the buffer.
		
		// Set up shader.
		m_pShader->begin();
		m_pShader->setUniform("u_ViewProjectionMatrix", m_viewProjectionMatrix);

		// Draw the buffer.
		GLenum mode = m_shapeType == ShapeType::FILLED ? GL_TRIANGLES
					: m_shapeType == ShapeType::LINE ? GL_LINES
					: GL_POINTS;
		kr_glCall(glDrawArrays(mode, 0, vertexNum));

		// Restore gl state.
		m_arrayBuffer.unbind();
		m_pShader->end();
		m_data.clear();
		m_shapeType = ShapeType::NONE;
	}

}