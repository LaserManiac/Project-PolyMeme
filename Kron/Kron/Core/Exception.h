#pragma once

#include "String.h"

// Allows for construction of simple named exception subtypes.
#define kr_makeExcept(_Name, _Super) class _Name : public _Super { public: _Name(const kr::String &_message) : _Super(_message) {} }

// =======================================================
//  Base exception class and some common Kron exceptions.
// =======================================================

namespace kr {

	class Exception {
	private:
		const kr::String m_message;

	public:
		Exception(const kr::String &_message)
			: m_message(_message) {}

		const kr::String getMessage() const { return m_message; }
	};

	kr_makeExcept(InvalidArgumentException, Exception);
	kr_makeExcept(InvalidStateException, Exception);
	kr_makeExcept(InvalidFormatException, Exception);
	kr_makeExcept(FileNotFoundException, Exception);
	kr_makeExcept(UnsupportedOperationException, Exception);
	kr_makeExcept(IndexOutOfBoundsException, Exception);
	kr_makeExcept(InvalidFileException, Exception);

}
