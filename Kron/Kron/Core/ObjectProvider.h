#pragma once

#include <vector>

#include "Class.h"
#include "Exception.h"

namespace kr {

	/*
	// A simple interface for providing objects.
	// An implementation example would be creating objects on obtain and deleting them on release.
	// Another example would be using a pool from which to obtain or release objects.
	*/
	template<class _Type>
	class ObjectProvider : public kr::NonCopyable, public kr::Deleteable {
	public:
		virtual _Type* obtain() = 0;
		virtual void release(_Type*) = 0;
		virtual void detach(_Type* _pObject) {
			throw kr::UnsupportedOperationException(kr::StringUtils::build("This implementation of ObjectProvider does not support object detaching."));
		}
	};



	/*
	// An exception thrown when an ObjectProvider instance does not own the provided object.
	*/
	class ObjectNotAttachedException : public kr::InvalidStateException {
	public:
		ObjectNotAttachedException(const void* _adress)
			: InvalidStateException(kr::StringUtils::build("Object at ", _adress, " is not attached to this ObjectProvider.")) {}
	};



	/*
	// A simple implementation of ObjectProvider which allocates and deallocates an object on the heap upon request.
	// It also keeps a list of allocated objects so it can delete them when it is deleted.
	*/
	template<class _Type>
	class HeapObjectProvider : public ObjectProvider<_Type> {
	private:
		std::vector<_Type*> m_objects;

	public:
		~HeapObjectProvider() {
			for (_Type* &pObject : m_objects)
				delete pObject;
		}

		_Type* obtain() override {
			_Type* pObject = new _Type();
			m_objects.push_back(pObject);
			return pObject;
		}

		void release(_Type* _pObject) override {
			auto iter = std::find(m_objects.begin(), m_objects.end(), _pObject);
			if (iter == m_objects.end())
				throw ObjectNotAttachedException(_pObject);
			std::swap(*iter, m_objects.back());
			m_objects.pop_back();
			delete _pObject;
		}

		void detach(_Type* _pObject) override {
			auto iter = std::find(m_objects.begin(), m_objects.end(), _pObject);
			if (iter == m_objects.end())
				throw ObjectNotAttachedException(_pObject);
			std::swap(*iter, m_objects.back());
			m_objects.pop_back();
		}

	};

}