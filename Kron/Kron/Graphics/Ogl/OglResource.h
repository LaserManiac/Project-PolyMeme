#pragma once

#include "Core\Class.h"
#include "Graphics\Graphics.h"

namespace kr {

	// ==================================================
	//  An OpenGL resource containing a resource handle.
	// ==================================================
	class GLResource : public kr::Deleteable, public kr::NonCopyable {
	protected:
		GLhandle m_handle;

	public:
		// Default constructor
		GLResource()
			: m_handle(GL_NONE) {}

		// Returns the openGL handle of the resource
		GLhandle getHandle() const { return m_handle; }

		// Returns true if the openGL handle is a valid handle
		bool isValid() const { return m_handle != GL_NONE; }

		// A virtual function called when the OpenGL context is lost.
		// This is called after a new context has been created.
		virtual void reload() {}

	};

}