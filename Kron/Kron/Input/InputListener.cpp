#include "InputListener.h"

namespace kr {

	InputMultiplexer::~InputMultiplexer() {
		for (const InputListener* listener : m_managedListeners)
			delete listener;
	}

	void InputMultiplexer::addListener(InputListener* _pListener, kr::Bool _managed) {
		m_listeners.emplace_back(_pListener);
		if (_managed)
			m_managedListeners.emplace_back(_pListener);
	}

	void InputMultiplexer::removeListener(InputListener* _pListener) {
		auto iter = std::find(m_listeners.begin(), m_listeners.end(), _pListener);
		if (iter != m_listeners.end())
			m_listeners.erase(iter);

		auto managedIter = std::find(m_managedListeners.begin(), m_managedListeners.end(), _pListener);
		if (managedIter != m_managedListeners.end())
			m_managedListeners.erase(managedIter);
	}

	kr::Bool InputMultiplexer::keyPressed(Keycode _key, kr::Bool _ctrl, kr::Bool _alt, kr::Bool _shift) {
		for (InputListener* listener : m_listeners)
			if (listener->keyPressed(_key, _ctrl, _alt, _shift))
				return true;
		return false;
	}

	kr::Bool InputMultiplexer::keyReleased(Keycode _key, kr::Bool _ctrl, kr::Bool _alt, kr::Bool _shift) {
		for (InputListener* listener : m_listeners)
			if (listener->keyReleased(_key, _ctrl, _alt, _shift))
				return true;
		return false;
	}

	kr::Bool InputMultiplexer::mouseButtonPressed(MouseButton _button, kr::u16 _x, kr::u16 _y) {
		for (InputListener* listener : m_listeners)
			if (listener->mouseButtonPressed(_button, _x, _y))
				return true;
		return false;
	}

	kr::Bool InputMultiplexer::mouseButtonReleased(MouseButton _button, kr::u16 _x, kr::u16 _y) {
		for (InputListener* listener : m_listeners)
			if (listener->mouseButtonReleased(_button, _x, _y))
				return true;
		return false;
	}

	kr::Bool InputMultiplexer::mouseWheelScrolled(kr::u8 _amount) {
		for (InputListener* listener : m_listeners)
			if (listener->mouseWheelScrolled(_amount))
				return true;
		return false;
	}

	kr::Bool InputMultiplexer::mouseMoved(kr::u16 _x, kr::u16 _y) {
		for (InputListener* listener : m_listeners)
			if (listener->mouseMoved(_x, _y))
				return true;
		return false;
	}

	kr::Bool InputMultiplexer::textTyped(kr::u32 _character) {
		for (InputListener* listener : m_listeners)
			if (listener->textTyped(_character))
				return true;
		return false;
	}



	kr::Bool ComboListener::keyPressed(Keycode _key, kr::Bool _ctrl, kr::Bool _alt, kr::Bool _shift) {
		if (_key == m_key.getKeycode() && _ctrl == m_key.hasCtrl() && _alt == m_key.hasAlt() && _shift == m_key.hasShift()) {
			m_pressed = true;
			return m_callback(m_pressed);
		}
		return false;
	}

	kr::Bool ComboListener::keyReleased(Keycode _key, kr::Bool _ctrl, kr::Bool _alt, kr::Bool _shift) {
		if (_key == m_key.getKeycode() && m_pressed) {
			m_pressed = false;
			return m_callback(m_pressed);
		}
		return false;
	}

}