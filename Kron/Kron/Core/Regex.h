#pragma once

#include <regex>

#include "..\Types.h"
#include "String.h"

namespace kr {

	// Define regex types
	using Regex = typename std::basic_regex<Char>;
	using RegexIterator = typename std::regex_iterator<String::const_iterator>;
	using RegexMatch = typename std::match_results<String::const_iterator>;

}
