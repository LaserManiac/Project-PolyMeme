#pragma once

#include <vector>

#include "..\..\Files.h"
#include "..\Graphics.h"

namespace kr::gfx {

	/*
	// A layout of an array buffer containing one or more attribute layouts
	*/
	class OglBufferLayout {
	private:
		GLenum m_usage;
		GLsizei m_vertexByteSize;
		std::vector<AttributeLayout> m_attributeLayouts;

	public:
		// _usage : The usage for the buffer - GL_STATIC_DRAW, GL_DYNAMIC_DRAW, etc.
		OglBufferLayout(GLenum _usage);

		GLenum getUsage() const;
		GLsizei getVertexByteSize() const;
		const std::vector<AttributeLayout>& getAttributeLayouts() const;

		// Adds a new attribute layout to the buffer layout
		OglBufferLayout& addAttributeLayout(const AttributeLayout &_attribute);

	};



	/*
	// A set of data to be uploaded to an array buffer
	*/
	class BufferData : public NonCopyable {
	private:
		std::vector<GLbyte> m_data;

	public:
		// _data : Pointer to data to be copied into this object
		// _size : Size of data in bytes
		BufferData(GLbyte* _pData, GLsizei _size);

		BufferData(BufferData &&_other);
		BufferData& operator=(BufferData &&_other);

		const std::vector<GLbyte>& getData() const;
		GLsizei getDataSize() const;

	};



	/*
	// An object holding buffer data and buffer layout for a single buffer in a mesh
	*/
	class MeshBuffer {
	private:
		BufferData m_data;
		OglBufferLayout m_layout;

	public:
		// _data : Buffer data object, it will be moved into this object!
		// _layout : Layout for this buffer
		MeshBuffer(BufferData &_data, const OglBufferLayout &_layout);

		MeshBuffer(MeshBuffer &&_other);
		MeshBuffer& operator=(MeshBuffer &&_other);

		const BufferData& getBufferData() const;
		const OglBufferLayout& getBufferLayout() const;
	};



	/*
	// A reference to a set of data to be uploaded to an index buffer
	*/
	class IndexData : public NonCopyable {
	private:
		std::vector<GLbyte> m_data;
		GLsizei m_indexSize;
		GLenum m_indexType;

	public:
		// _data : Pointer to data to be copied into this object
		// _indexSize : Size of an index in bytes
		// _indexNumber : Number of indices
		// _indexType : GL data type of the index, eg. GL_UNSIGNED_SHORT
		IndexData(GLbyte* _data, GLsizei _indexSize, GLsizei _indexNumber, GLenum _indexType);

		IndexData(IndexData &&_other);
		IndexData& operator=(IndexData &&_other);

		const std::vector<GLbyte>& getData() const;
		GLsizei getIndexSize() const;
		GLsizei getIndexNumber() const;
		GLsizei getDataSize() const;
		GLenum getIndexType() const;

	};



	/*
	// A set of buffer data, an index buffer, and primitive type of a mesh
	*/
	class MeshData {
	private:
		std::vector<MeshBuffer> m_meshBuffers;
		IndexData m_indexData;
		GLenum m_primitiveType;

	public:
		// _indexData : Index data object, it will be moved into this object!
		// _primitiveType : OpenGL primitive type (eg. GL_LINES, GL_TRIANGLES)
		MeshData(IndexData &_indexData, GLenum _primitiveType);

		const std::vector<MeshBuffer>& getMeshBuffers() const;
		const IndexData& getIndexData() const;
		GLenum getPrimitiveType() const;

		// Add new buffer data to mesh data
		MeshData& addMeshBuffer(MeshBuffer &_bufferData);


	public:
		static MeshData* loadFromJMesh(const kr::files::path &_path);
	};

}