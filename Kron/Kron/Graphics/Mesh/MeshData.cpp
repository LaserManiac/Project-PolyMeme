#include "MeshData.h"

#include <vector>

#include "..\..\Json.h"

namespace kr::gfx {

	BufferData::BufferData(GLbyte* _data, GLsizei _size) {
		m_data = std::vector<GLbyte>(_data, _data + _size);
	}

	BufferData::BufferData(BufferData &&_other) {
		m_data = std::move(_other.m_data);
	}

	BufferData& BufferData::operator=(BufferData &&_other) {
		m_data = std::move(_other.m_data);
		return *this;
	}

	const std::vector<GLbyte>& BufferData::getData() const { return m_data; }

	GLsizei BufferData::getDataSize() const { return m_data.size(); }



	MeshBuffer::MeshBuffer(BufferData &_data, const OglBufferLayout &_layout)
		: m_data(std::move(_data)), m_layout(_layout) {}

	MeshBuffer::MeshBuffer(MeshBuffer &&_other) 
		: m_data(std::move(_other.m_data)), m_layout(_other.m_layout) {}

	MeshBuffer& MeshBuffer::operator=(MeshBuffer &&_other) {
		m_data = std::move(_other.m_data);
		m_layout = _other.m_layout;
		return *this;
	}

	const BufferData& MeshBuffer::getBufferData() const { return m_data; }

	const OglBufferLayout& MeshBuffer::getBufferLayout() const { return m_layout; }



	IndexData::IndexData(GLbyte* _data, GLsizei _indexSize, GLsizei _indexNumber, GLenum _indexType)
		: m_indexSize(_indexSize), m_indexType(_indexType) {
		
		m_data = std::vector<GLbyte>(_data, _data + _indexSize * _indexNumber);
	}

	IndexData::IndexData(IndexData &&_other) {
		m_indexSize = _other.m_indexSize;
		m_indexType = _other.m_indexType;
		m_data = std::move(_other.m_data);
	}

	IndexData& IndexData::operator=(IndexData &&_other) {
		m_indexSize = _other.m_indexSize;
		m_indexType = _other.m_indexType;
		m_data = std::move(_other.m_data);
		return *this;
	}

	const std::vector<GLbyte>& IndexData::getData() const { return m_data; }

	GLsizei IndexData::getIndexSize() const { return m_indexSize; }

	GLsizei IndexData::getIndexNumber() const { return m_data.size() / m_indexSize; }

	GLsizei IndexData::getDataSize() const { return m_data.size(); }

	GLenum IndexData::getIndexType() const { return m_indexType; }



	MeshData::MeshData(IndexData& _indexData, GLenum _primitiveType)
		: m_indexData(std::move(_indexData)), m_primitiveType(_primitiveType) {}

	const std::vector<MeshBuffer>& MeshData::getMeshBuffers() const { return m_meshBuffers; }

	const IndexData& MeshData::getIndexData() const { return m_indexData; }

	GLenum MeshData::getPrimitiveType() const { return m_primitiveType; }

	MeshData& MeshData::addMeshBuffer(MeshBuffer &_bufferData) {
		m_meshBuffers.push_back(std::move(_bufferData));
		return *this;
	}



	MeshData* MeshData::loadFromJMesh(const kr::files::path &_path) {
		kr::Json jsonData;

		try {
			std::ifstream in(_path);
			in >> jsonData;
				
			GLenum type;
			kr::s8 jsonType = jsonData["type"].get<kr::s8>();
			if (jsonType == "TRIANGLES")
				type = GL_TRIANGLES;
			else if (jsonType == "LINES")
				type = GL_LINES;
			else if (jsonType == "POINTS")
				type = GL_POINTS;
			else
				throw std::exception(kr::s8(jsonType + " is not a valid type").c_str());

			std::vector<GLushort> indexVector = jsonData["indices"];
			IndexData indexData((GLbyte*)&indexVector.front(), sizeof(GLushort), indexVector.size(), GL_UNSIGNED_SHORT);

			MeshData* meshData = new MeshData(indexData, type);

			for (auto &buffer : jsonData["vertices"]) {
					
				OglBufferLayout bufferLayout(GL_STATIC_DRAW);
				for (auto &attr : buffer["layout"]) {
					kr::s8 attrStr = attr.get<kr::s8>();
					if (attrStr == "POSITION_3D")
						bufferLayout.addAttributeLayout(AttributeLayout::POSITION_3D);
					else if(attrStr == "POSITION_2D")
						bufferLayout.addAttributeLayout(AttributeLayout::POSITION_2D);
					else if (attrStr == "COLOR_RGB")
						bufferLayout.addAttributeLayout(AttributeLayout::COLOR_RGB);
					else if (attrStr == "COLOR_RGBA")
						bufferLayout.addAttributeLayout(AttributeLayout::COLOR_RGBA);
					else
						throw std::exception(kr::s8(attrStr + " is not a valid attribute").c_str());
				}

				std::vector<GLfloat> bufferDataVector = buffer["data"];
				BufferData bufferData((GLbyte*)&bufferDataVector.front(), bufferDataVector.size() * sizeof(GLfloat));

				MeshBuffer meshBuffer(bufferData, bufferLayout);
				meshData->addMeshBuffer(meshBuffer);
			}

			return meshData;

		} catch (const std::exception &e) {
			std::cout << "Exception: " << e.what() << "\n";
		}

		return NULL;
	}

}
