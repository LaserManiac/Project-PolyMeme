#pragma once

#include <string>

#include <SFML\Graphics\Image.hpp>

#include "GlTexture.h"

namespace kr {

	class Texture2D : public GlTexture<GL_TEXTURE_2D> {

	private:
		GLsizei m_width;
		GLsizei m_height;

	public:
		Texture2D(GLsizei _width, GLsizei _height, GLenum _type)
			: m_width(_width), m_height(_height), kr::GlTexture<GL_TEXTURE_2D>(_type) {};

		GLsizei getWidth() const;
		GLsizei getHeight() const;

		void loadData(GLint _level, GLenum _format, GLenum _type, const GLvoid* _data);

	};

}