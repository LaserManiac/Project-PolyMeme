﻿#include "VertexLayout.h"

#include <iomanip>

#include "Core\Exception.h"
#include "Core\String.h"

namespace kr {

	VertexLayout::VertexLayout(const std::vector<VertexAttribute>& _attributes)
		: m_attributes(std::move(_attributes)),
		m_byteSize(0) {
		
		// Add up attribute byte sizes.
		for (const VertexAttribute &attr : m_attributes)
			m_byteSize += attr.getByteSize();
	}

	VertexLayout* VertexLayout::loadFromJVL(const kr::FilePath &_path) {
		kr::Json json;
		kr::JsonUtils::loadFromFile(json, _path);
		return createFromJVL(json);
	}

	VertexLayout* VertexLayout::createFromJVL(const kr::Json &_json) {
		std::vector<VertexAttribute> attribs;
		try {
			kr::Json data = _json["data"];
			for (const kr::Json &attr : data) {
				kr::u8 size = attr["size"];
				kr::Bool nor = attr["normalized"];
				VertexAttribute::Type type = attr["type"];
				VertexAttribute::Usage usage = attr["usage"];
				kr::String name = attr["name"];
				attribs.emplace_back(type, size, nor, usage, name);
			}
		}
		catch (const kr::Json::exception &e) {
			throw kr::InvalidFormatException(kr::str("Could not load JVL file: ", e.what()));
		}
		catch (const kr::Exception &e) {
			throw kr::InvalidFormatException(kr::str("Could not load JVL file: ", e.getMessage()));
		}
		catch (...) {
			throw kr::InvalidFormatException("Could not load JVL file.");
		}
		return new kr::VertexLayout(attribs);
	}

	kr::u8 VertexAttribute::getTypeBytes(Type _type) {
		switch (_type) {
		case FLOAT:
		case INT:
		case UINT:
			return 4;
		case SHORT:
		case USHORT:
			return 2;
		case BYTE:
		case UBYTE:
			return 1;
		default:
			return 0;
		}
	}

	std::ostream& operator<<(std::ostream &_out, const VertexLayout &_layout) {
		kr::u32 maxNameSize = 0;
		for (const auto &a : _layout.getAttributes())
			maxNameSize = std::max(maxNameSize, a.getName().length());

		_out << "+-" << std::setw(maxNameSize) << std::setfill('-') << "" << "-+------+------------+----------------+" << std::setfill(' ') << std::resetiosflags(0) << std::endl;
		_out << "| " << std::setw(maxNameSize) << "name" << std::resetiosflags(0) << " | size | normalized | static/dynamic |" << std::endl;
		_out << "+-" << std::setw(maxNameSize) << std::setfill('-') << "" << "-+------+------------+----------------+" << std::setfill(' ') << std::resetiosflags(0) << std::endl;
		for (const auto &a : _layout.getAttributes()) {
			_out << "| " << std::setw(maxNameSize) << std::right << a.getName() << std::resetiosflags(0) << " | ";
			_out << std::setw(4) << std::left << (int)a.getByteSize() << std::resetiosflags(0) << " | ";
			_out << std::setw(10) << std::left << (a.isNormalized() ? "Yes" : "No") << std::resetiosflags(0) << " | ";
			_out << std::setw(14) << std::left << (a.getUsage() == kr::VertexAttribute::STATIC ? "Static" : "Dynamic") << std::resetiosflags(0) << " |" << std::endl;
			_out << "+-" << std::setw(maxNameSize) << std::setfill('-') << "" << "-+------+------------+----------------+" << std::setfill(' ') << std::resetiosflags(0) << std::endl;
		}

		return _out;
	}

}

namespace nlohmann {

	// Json serializer for kr::VertexLayout::Type.
	// For more info, see nlohman::json docs in Json.h.
	template<>
	class adl_serializer<kr::VertexAttribute::Type> {
	public:
		static void to_json(json &_json, const kr::VertexAttribute::Type &_type) {
			switch (_type) {
			case kr::VertexAttribute::FLOAT:	_json = "FLOAT";	break;
			case kr::VertexAttribute::INT:		_json = "INT";		break;
			case kr::VertexAttribute::UINT:		_json = "UINT";		break;
			case kr::VertexAttribute::SHORT:	_json = "SHORT";	break;
			case kr::VertexAttribute::USHORT:	_json = "USHORT";	break;
			case kr::VertexAttribute::BYTE:		_json = "BYTE";		break;
			case kr::VertexAttribute::UBYTE:	_json = "UBYTE";	break;
			default: throw kr::InvalidStateException("Invalid vertex attribute element type."); break;
			}
		}

		static void from_json(const json &_json, kr::VertexAttribute::Type &_type) {
			if (_json == "FLOAT")
				_type = kr::VertexAttribute::FLOAT;
			else if (_json == "INT")
				_type = kr::VertexAttribute::INT;
			else if (_json == "UINT")
				_type = kr::VertexAttribute::UINT;
			else if (_json == "SHORT")
				_type = kr::VertexAttribute::SHORT;
			else if (_json == "USHORT")
				_type = kr::VertexAttribute::USHORT;
			else if (_json == "BYTE")
				_type = kr::VertexAttribute::BYTE;
			else if (_json == "UBYTE")
				_type = kr::VertexAttribute::UBYTE;
			else
				throw kr::InvalidFormatException(kr::str(_json, " is not a valid vertex attribute element type."));
		}
	};

	// Json serializer for kr::VertexLayout::Usage.
	// For more info, see nlohman::json docs in Json.h.
	template<>
	class adl_serializer<kr::VertexAttribute::Usage> {
	public:
		static void to_json(json &_json, const kr::VertexAttribute::Usage &_usage) {
			switch (_usage) {
			case kr::VertexAttribute::STATIC: _json = "STATIC"; break;
			case kr::VertexAttribute::DYNAMIC: _json = "DYNAMIC"; break;
			default: throw kr::InvalidStateException("Invalid vertex attribute usage modifier.");
			}
		}

		static void from_json(const json &_json, kr::VertexAttribute::Usage &_usage) {
			if (_json == "STATIC")
				_usage = kr::VertexAttribute::STATIC;
			else if(_json == "DYNAMIC")
				_usage = kr::VertexAttribute::DYNAMIC;
			else throw kr::InvalidFormatException(kr::str(_json, " is not a valid vertex attribute usage modifier."));
		}
	};

}
