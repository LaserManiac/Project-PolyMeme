#pragma once

#include <iostream>

#include "Types.h"
#include "String.h"

namespace kr {

	class Logging {
	public:
		template<class... _Types>
		static void console(const _Types&... _message) {
			std::cout << kr::StringUtils::build(_message...);
		}

		template<class... _Types>
		static void consoleLine(const _Types&... _message) {
			std::cout << kr::StringUtils::build(_message...) << "\n";
		}

	};

}