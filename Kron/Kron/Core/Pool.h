#pragma once

#include <malloc.h>
#include <deque>
#include <set>
#include <unordered_set>

#include "Class.h"
#include "Types.h"
#include "Exception.h"
#include "Math.h"

namespace kr {

	template<class _Type>
	class MemoryPage {
	private:
		kr::u32 m_size;
		_Type* m_pData;

	public:
		MemoryPage(MemoryPage &&_other)
			:m_size(0), m_pData(NULL) {

			std::swap(m_size, _other.m_size);
			std::swap(m_pData, _other.m_pData);
		}

		MemoryPage& operator=(MemoryPage &&_other) {
			std::swap(m_size, _other.m_size);
			std::swap(m_pData, _other.m_pData);
		}

		MemoryPage(kr::u32 _size)
			: m_size(_size), m_pData(static_cast<_Type*>(malloc(static_cast<size_t>(sizeof(_Type) * m_size)))) {

			kr::Logging::consoleLine("Allocated new memory page at ", m_pData, " (Block: ", sizeof(_Type), " bytes, Size: ", m_size, " elements)");
		}

		~MemoryPage() {
			if (m_pData != NULL) {
				free(m_pData);
				kr::Logging::consoleLine("Deleted memory page at ", m_pData);
			}
		}

		kr::u32 getSize() const {
			return m_size;
		}

		kr::u32 getObjectIndex(_Type* _element) const {
			kr::u32 index = _element - m_pData;
			return index > m_size ? m_size : index;
		}

		kr::Bool containsObject(_Type* _element) const {
			return getObjectIndex(_element) != m_size;
		}

		_Type* getObject(kr::u32 _offset) const {
			return m_pData + _offset;
		}

		_Type* operator[](kr::u32 _offset) const {
			return getObject(_offset);
		}

	};



	template<class _Type>
	class ObjectPool : public kr::NonCopyable {
	private:
		using PageList = typename std::vector<MemoryPage<_Type>>;
		using FreeSet = typename std::set<kr::u32>;
		using TakenSet = typename std::unordered_set<kr::u32>;

	private:
		kr::u32 m_pageSize;
		kr::u32 m_capacity;
		PageList m_pages;
		FreeSet m_freeSlots;
		TakenSet m_takenSlots;

		MemoryPage<_Type>* allocatePage() {
			MemoryPage<_Type> &page = m_pages.emplace_back(m_pageSize);
			
			m_capacity = m_pages.size() * m_pageSize;
			m_takenSlots.reserve(m_capacity);

			kr::u32 pageIndexValue = (m_pages.size() - 1) * m_pageSize;
			for (kr::u32 i = 0; i < m_pageSize; i++)
				m_freeSlots.emplace(pageIndexValue + i);

			return &page;
		}

		_Type* getElement(kr::u32 _adress) {
			MemoryPage<_Type> &page = m_pages[_adress / m_pageSize];
			return page[_adress % m_pageSize];
		}

		kr::u32 getAdress(_Type* _pObject) {
			kr::u32 memoryPageIndex = 0;
			for (const MemoryPage<_Type> &page : m_pages) {
				if (page.containsObject(_pObject)) {
					return memoryPageIndex * m_pageSize + page.getObjectIndex(_pObject);
				}
				memoryPageIndex++;
			}
			return m_capacity;
		}

		kr::u32 obtainAdress() {
			if (m_freeSlots.empty())
				allocatePage();

			kr::u32 adress = *m_freeSlots.begin();
			m_freeSlots.erase(m_freeSlots.begin());
			m_takenSlots.emplace(adress);

			return adress;
		}

		kr::Bool releaseAdress(kr::u32 _adress) {
			TakenSet::iterator index = m_takenSlots.find(_adress);
			if (index == m_takenSlots.end())
				return true;
			
			m_takenSlots.erase(index);
			m_freeSlots.emplace(_adress);
			
			return false;
		}

	public:
		ObjectPool(kr::u32 _pageSize)
			: m_pageSize(_pageSize) {

			allocatePage();

		}

		~ObjectPool() {
			kr::Logging::consoleLine("Deleting ", m_takenSlots.size(), " elements...");
			for (const kr::u32 &adress : m_takenSlots)
				getElement(adress)->~_Type();

			kr::Logging::consoleLine("Deleting ", m_pages.size(), " pages...");
			m_pages.clear();
		}

		kr::u32 getCapacity() const { return m_capacity; }

		_Type* obtain() {
			kr::u32 adress = obtainAdress();
			_Type* pObject = getElement(adress);
			return pObject;
		}

		void release(_Type* _pObject) {
			kr::u32 adress = getAdress(_pObject);

			if (adress == m_capacity)
				throw kr::InvalidArgumentException(kr::StringUtils::build("Object at adress ", _pObject, " does not belong to this pool."));
			
			if(releaseAdress(adress))
				throw kr::InvalidArgumentException(kr::StringUtils::build("Object at adress ", _pObject, " is not currently obtained."));

			_pObject->~_Type();
		}

	};

}