#pragma once

// ===============================================================
//  A collection of classes intended to be used as superclasses
//  to modify the behaviour of their subclasses in a certain way.
// ===============================================================

namespace kr {

	// ===============================================
	//  A utility class that disables the constructor
	// ===============================================
	class NonConstructible {
	public:
		NonConstructible() = delete;
	};



	// =====================================================================================
	//  A utility class that disables the default copy constructor and assignment operator.
	// =====================================================================================
	class NonCopyable {
	protected:
		NonCopyable() {};

	private:
		NonCopyable(const NonCopyable&) {};
		NonCopyable& operator=(const NonCopyable&) {};
	};



	// ===========================================================================
	//  A utility class that makes the constructor protected.
	//  This allows creation of objects of this class only from inside the class.
	//  If you want real private constructible, use private inheritance.
	// ===========================================================================
	class PrivateConstructible {
	protected:
		PrivateConstructible() {}
	};



	// =====================================================================================================
	//  A utility class that provides a virtual destructor.
	//  This gives a superclass from which to call the destructor if the object type is unknown at runtime.
	// =====================================================================================================
	class Deleteable {
	public:
		virtual ~Deleteable() {}
	};

}

