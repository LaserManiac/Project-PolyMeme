#include "OglShader.h"

#include <sstream>
#include <fstream>

#include "Core\Exception.h"
#include "Core\Logging.h"

namespace kr {

	OglShaderStage::OglShaderStage(GLenum _stage, const kr::String &_source)
		: m_stage(_stage) {

		// Create shader stage.
		kr_glCall(m_handle = glCreateShader(_stage));

		// Upload program source.
		const char* pSource = _source.c_str();
		kr_glCall(glShaderSource(m_handle, 1, &pSource, NULL));

		// Compile shader stage.
		kr_glCall(glCompileShader(m_handle));

		// Download compile log.
		GLint status;
		kr_glCall(glGetShaderiv(m_handle, GL_COMPILE_STATUS, &status));
		if (status != GL_TRUE) {
			m_compiled = false;
			
			GLint logLength;
			kr_glCall(glGetShaderiv(m_handle, GL_INFO_LOG_LENGTH, &logLength));

			if (logLength > 0) {
				std::vector<GLchar> log(logLength);
				kr_glCall(glGetShaderInfoLog(m_handle, logLength, nullptr, &log[0]));
				m_compileLog = kr::String(&log[0]);
			}
		} else {
			m_compiled = true;
			m_compileLog = "Shader compiled successfuly.";
		}

	}

	OglShaderStage::~OglShaderStage() {
		kr_glCall(glDeleteShader(m_handle));
	}

	kr::Bool OglShaderStage::isCompiled() const {
		return m_compiled;
	}

	const kr::String& OglShaderStage::getCompileLog() const {
		return m_compileLog;
	}





	OglShaderProgram::OglShaderProgram(const std::vector<OglShaderStage*> &_stages) {

		// Create the shader program.
		kr_glCall(m_handle = glCreateProgram());

		// Attach all stages.
		for (const OglShaderStage* stage : _stages)
			kr_glCall(glAttachShader(m_handle, stage->getHandle()));

		// Link shader program.
		kr_glCall(glLinkProgram(m_handle));

		// Download the link log.
		GLint linkStatus;
		kr_glCall(glGetProgramiv(m_handle, GL_LINK_STATUS, &linkStatus));
		if (linkStatus != GL_TRUE) {
			m_linked = false;

			GLint logLength;
			kr_glCall(glGetProgramiv(m_handle, GL_INFO_LOG_LENGTH, &logLength));
			
			if (logLength > 0) {
				std::vector<GLchar> log(logLength);
				kr_glCall(glGetProgramInfoLog(m_handle, logLength, nullptr, &log[0]));
				m_linkLog = kr::String(&log[0]);
			}
		} else {
			m_linked = true;
			m_linkLog = "Shader linked successfuly.";
		}

		// Detach all shader stages.
		for (const OglShaderStage* stage : _stages)
			kr_glCall(glDetachShader(m_handle, stage->getHandle()));

		// Cache shader resources.
		if (isLinked()) {
			cacheAttributes();
			cacheUniforms();
		}
	}

	OglShaderProgram::~OglShaderProgram() {
		kr_glCall(glDeleteProgram(m_handle));
	}

	GLint OglShaderProgram::getAttributeLocation(const kr::String &_name) const {
		auto index = m_attributeCache.find(_name);
		if (index == m_attributeCache.end())
			return -1;
		return index->second.location;
	}

	GLint OglShaderProgram::getUniformLocation(const kr::String &_name) const {
		auto index = m_uniformCache.find(_name);
		if (index == m_uniformCache.end())
			return -1;
		return index->second.location;
	}

	void OglShaderProgram::begin() {
		kr_glCall(glUseProgram(m_handle));
	}

	void OglShaderProgram::end() {
		kr_glCall(glUseProgram(GL_NONE));
	}

	void OglShaderProgram::setUniform(kr::i32 _index, kr::i32 _value)  {
		kr_glCall(glUniform1i(_index, _value));
	}

	void OglShaderProgram::setUniform(kr::i32 _index, kr::u32 _value) {
		kr_glCall(glUniform1ui(_index, _value));
	}

	void OglShaderProgram::setUniform(kr::i32 _index, kr::f32 _value) {
		kr_glCall(glUniform1f(_index, _value));
	}

	void OglShaderProgram::setUniform(kr::i32 _index, const gmtl::Vec2i &_value) {
		kr_glCall(glUniform2iv(_index, 1, _value.getData()));
	}

	void OglShaderProgram::setUniform(kr::i32 _index, const gmtl::Vec2u &_value) {
		kr_glCall(glUniform2uiv(_index, 1, _value.getData()));
	}

	void OglShaderProgram::setUniform(kr::i32 _index, const gmtl::Vec2f &_value) {
		kr_glCall(glUniform2fv(_index, 1, _value.getData()));
	}

	void OglShaderProgram::setUniform(kr::i32 _index, const gmtl::Vec3i &_value) {
		kr_glCall(glUniform3iv(_index, 1, _value.getData()));
	}

	void OglShaderProgram::setUniform(kr::i32 _index, const gmtl::Vec3u &_value) {
		kr_glCall(glUniform3uiv(_index, 1, _value.getData()));
	}

	void OglShaderProgram::setUniform(kr::i32 _index, const gmtl::Vec3f &_value) {
		kr_glCall(glUniform3fv(_index, 1, _value.getData()));
	}

	void OglShaderProgram::setUniform(kr::i32 _index, const gmtl::Vec4i &_value) {
		kr_glCall(glUniform4iv(_index, 1, _value.getData()));
	}

	void OglShaderProgram::setUniform(kr::i32 _index, const gmtl::Vec4u &_value) {
		kr_glCall(glUniform4uiv(_index, 1, _value.getData()));
	}

	void OglShaderProgram::setUniform(kr::i32 _index, const gmtl::Vec4f &_value) {
		kr_glCall(glUniform4fv(_index, 1, _value.getData()));
	}

	void OglShaderProgram::setUniform(kr::i32 _index, const gmtl::Matrix33f &_value) {
		kr_glCall(glUniformMatrix3fv(_index, 1, GL_FALSE, _value.getData()));
	}

	void OglShaderProgram::setUniform(kr::i32 _index, const gmtl::Matrix44f &_value) {
		kr_glCall(glUniformMatrix4fv(_index, 1, GL_FALSE, _value.getData()));
	}

	void OglShaderProgram::cacheAttributes() {

		// Get number of attributes.
		GLint attribNum;
		kr_glCall(glGetProgramiv(m_handle, GL_ACTIVE_ATTRIBUTES, &attribNum));
		
		// Get max size of an attribute name.
		GLsizei maxAttribNameLen;
		kr_glCall(glGetProgramiv(m_handle, GL_ACTIVE_ATTRIBUTE_MAX_LENGTH, &maxAttribNameLen));

		std::vector<GLchar> attribName(maxAttribNameLen + 1);
		GLsizei attribLength;
		GLsizei attribSize;
		GLenum attribType;
		GLint attribLocation;

		for (GLint attribIndex = 0; attribIndex < attribNum; attribIndex++) {

			// Get attribute data.
			kr_glCall(glGetActiveAttrib(m_handle, attribIndex, maxAttribNameLen, &attribLength, &attribSize, &attribType, &attribName[0]));

			// If the attribute is an array:
			if (attribSize > 1) {
				// OpenGL spec says name returns just the name of the array.
				// But some drivers return the name with [0] appended at the end.
				// Because fuck consistency, right?
				// So we check for that here and remove it if it's present.
				trimResourceName(attribName);

				// Go through the array and get the location of each array element.
				for (GLsizei i = 0; i < attribSize; i++) {
					kr::String attribElementName = kr::str(&attribName[0], "[", i, "]");
					kr_glCall(attribLocation = glGetAttribLocation(m_handle, attribElementName.c_str()));
					if (attribLocation != -1)
						m_attributeCache.emplace(attribElementName.c_str(), Attribute(attribLocation, attribType));
				}
			} else {
				// Cache the attribute.
				kr_glCall(attribLocation = glGetAttribLocation(m_handle, &attribName[0]));
				if (attribLocation != -1)
					m_attributeCache.emplace(&attribName[0], Attribute(attribLocation, attribType));
			}

		}

	}

	void OglShaderProgram::cacheUniforms() {

		// Get number of uniforms.
		GLint uniformNum;
		kr_glCall(glGetProgramiv(m_handle, GL_ACTIVE_UNIFORMS, &uniformNum));

		// Get max size of an uniform name.
		GLsizei maxUniformNameLen;
		kr_glCall(glGetProgramiv(m_handle, GL_ACTIVE_UNIFORM_MAX_LENGTH, &maxUniformNameLen));

		std::vector<GLchar> uniformName(maxUniformNameLen + 1);
		GLsizei uniformLength;
		GLsizei uniformSize;
		GLenum uniformType;
		GLint uniformLocation;

		for (GLint uniformIndex = 0; uniformIndex < uniformNum; uniformIndex++) {

			// Get uniform data.
			kr_glCall(glGetActiveUniform(m_handle, uniformIndex, maxUniformNameLen, &uniformLength, &uniformSize, &uniformType, &uniformName[0]));

			// If the uniform is an array:
			if (uniformSize > 1) {
				// OpenGL spec says name returns just the name of the array.
				// But some drivers return the name with [0] appended at the end.
				// Because fuck consistency, right?
				// So we check for that here and remove it if it's present.
				trimResourceName(uniformName);

				// Go through the array and get the location of each array element.
				for (GLsizei i = 0; i < uniformSize; i++) {
					kr::String uniformElementName = kr::str(&uniformName[0], "[", i, "]");
					kr_glCall(uniformLocation = glGetUniformLocation(m_handle, uniformElementName.c_str()));
					if (uniformLocation != -1)
						m_uniformCache.emplace(uniformElementName.c_str(), Uniform(uniformLocation, uniformType));
				}
			}
			else {
				// Cache the uniform.
				kr_glCall(uniformLocation = glGetUniformLocation(m_handle, &uniformName[0]));
				if (uniformLocation != -1)
					m_uniformCache.emplace(&uniformName[0], Uniform(uniformLocation, uniformType));
			}

		}
	}

	void OglShaderProgram::trimResourceName(std::vector<GLchar> &_name) {
		auto index = std::find(_name.begin(), _name.end(), '[');
		if (index != _name.end()) {
			_name.erase(index, _name.end());
			_name.emplace_back('\0');
		}
	}
	
	OglShaderProgram* OglShaderProgram::loadFromKSS(const kr::FilePath &_path) {
		// Open file.
		std::ifstream in(_path);
		if(!in)
			throw kr::FileNotFoundException(kr::str("Could not open file ", _path));

		// Read file.
		std::unordered_map<GLenum, kr::StringStream> streams;
		kr::String line;
		kr::String version;
		GLenum type = GL_NONE;
		while (std::getline(in, line)) {
			if (line.empty() && type == GL_NONE)
				continue;
			else if (line.find("#version") == 0)
				version = line;
			else if (line == "#vertex")
				type = GL_VERTEX_SHADER;
			else if (line == "#fragment")
				type = GL_FRAGMENT_SHADER;
			else if (line == "#geometry")
				type = GL_GEOMETRY_SHADER;
			else if (type != GL_NONE)
				streams[type] << line << "\n";
			else
				throw kr::InvalidFormatException(kr::str("No shader stage was declared before line:\n", line));
		}

		// Prepend version tag to all stages.
		std::unordered_map<GLenum, kr::String> sources;
		for (const auto &entry : streams)
			sources[entry.first] = kr::str(version, "\n", entry.second.str());

		// Compile stages.
		std::vector<OglShaderStage*> stages;
		for (const auto &entry : sources)
			stages.emplace_back(new OglShaderStage(entry.first, entry.second));

		// Link into a shader.
		OglShaderProgram* shader = new OglShaderProgram(stages);

		// Delete stages.
		for (const OglShaderStage* stage : stages)
			delete stage;

		return shader;
	}

}
