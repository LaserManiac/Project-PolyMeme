#pragma once

#include <unordered_map>

#include "Graphics\Graphics.h"
#include "Graphics\Ogl\OglResource.h"

namespace kr  {

	// ============================================================================
	//  A generic OpenGL buffer
	//  _Target : Bind target (eg. GL_ARRAY_BUFFER, GL_ELEMENT_ARRAY_BUFFER, etc.)
	// ============================================================================
	template<GLenum _Target>
	class OglBuffer : public GLResource {
	public:
		OglBuffer() {
			kr_glCall(glGenBuffers(1, &m_handle));
		}

		~OglBuffer() {
			if(isValid())
				kr_glCall(glDeleteBuffers(1, &m_handle));
		}

		// Get bind target
		GLenum getTarget() const {
			return _Target;
		}

		// Bind buffer to target
		void bind() const {
			kr_glCall(glBindBuffer(_Target, m_handle));
		}

		// Bind GL_NONE to target.
		void unbind() const {
			kr_glCall(glBindBuffer(_Target, GL_NONE));
		}

		// Upload byte data to the buffer.
		//
		// [WARNING]: Buffer must be bound!
		//
		void uploadData(const void* _data, GLsizeiptr _size, GLenum _usage) {
			kr_glCall(glBufferData(_Target, _size, _data, _usage));
		}
			
		// Upload byte data to a part of the buffer.
		//
		// [WARNING]: Buffer must be bound!
		//
		void uploadSubData(const void* _data, GLsizeiptr _size, GLintptr _offset) {
			kr_glCall(glBufferSubData(_Target, _offset, _size, _data));
		}

		// Upload a vector of generic elements to the buffer
		//
		// [WARNING]: Buffer must be bound!
		//
		template<class _Type>
		void uploadData(const std::vector<_Type> &_data, GLenum _usage) {
			uploadData(&_data.at(0), _data.size() * sizeof(_Type), _usage);
		}

		// Upload a vector of generic elements to a part of the buffer.
		//
		// [WARNING]: Buffer must be bound!
		//
		template<class _Type>
		void uploadSubData(const std::vector<_Type> &_data, GLintptr _elementOffset) {
			uploadSubData(&_data.at(0), _data.size() * sizeof(_Type), _elementOffset * sizeof(_Type));
		}

	};

	using OglArrayBuffer = typename OglBuffer<GL_ARRAY_BUFFER>;
	using OglElementBuffer = typename OglBuffer<GL_ELEMENT_ARRAY_BUFFER>;

}
