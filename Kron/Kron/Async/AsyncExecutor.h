#pragma once

#include <list>
#include <functional>
#include <mutex>
#include <future>

#include <SFML\System\NonCopyable.hpp>

#include "Obsolete\Task.h"
#include "Async\Semaphore.h"
#include "Async\WorkerThread.h"
#include "Async\AsyncTaskQueue.h"

namespace kr {

	/*
	// Executes functions asynchronously on a configurable number of worker threads.
	// A std::future object is returned on execute().
	*/
	class AsyncExecutor : public sf::NonCopyable {
	private:
		AsyncTaskQueue m_taskQueue;
		Semaphore m_semaphore;
		std::list<WorkerThread> m_workerThreads;

	public:
		// Constructor that takes the number of worker threads to initialize.
		AsyncExecutor(int _threadCount = 1);

		// Destructor which closes the task queue,
		// waits for remaining tasks to finish,
		// and then stops the running worker threads.
		~AsyncExecutor();

		// Returns number of worker threads on this executor.
		int getThreadCount() const;

		// Returns the semaphore counter value, aka. how many tasks have not yet finished execution.
		// If there are tasks currently executing, which means they are not in the queue, they are still
		// counted towards the task count because they have not yet released the semaphore.
		int getTaskCount();

		// Blocks the caller thread until all tasks are completed.
		void waitForTasks();

		// Enqueues a new task with the provided function.
		// A std::future object is returned which will contain the result of the function,
		// or an exception pointer if one was thrown during the executon of the task.
		// The semaphore is obtained and it will be released once the task has reached it's end.
		template<class _Return>
		std::future<_Return> execute(const std::function<_Return()> &_function) {
			std::shared_ptr<std::promise<_Return>> pPromise = std::make_shared<std::promise<_Return>>();
			Task *pTask = constructTask<_Return>(_function);
			m_semaphore.obtain();
			m_taskQueue.enqueue(pTask);
			return pPromise->get_future();
		}

	private:
		// Creates a new Task from the provided function, together with the std::promise object that will hold the returned value.
		template<class _Return>
		Task* constructTask(const std::function<_Return()> &_function) {
			std::shared_ptr<std::promise<_Return>> pPromise = std::make_shared<std::promise<_Return>>();
			return new Task(std::bind([=](std::shared_ptr<std::promise<_Return>> _pPromise, std::function<_Return()> _function, Semaphore* _pSemaphore) {
				try {
					_pPromise->set_value(_function());
				} catch (...) {
					_pPromise->set_exception(std::current_exception());
				}
				_pSemaphore->release();
			}, pPromise, _function, &m_semaphore));
		}

		// A specialization of constructTask<>() for functions with void return type.
		template<>
		Task* constructTask(const std::function<void()> &_function) {
			std::shared_ptr<std::promise<void>> pPromise = std::make_shared<std::promise<void>>();
			return new Task(std::bind([=](std::shared_ptr<std::promise<void>> _pPromise, std::function<void()> _function, Semaphore* _pSemaphore) {
				try {
					_function();
					_pPromise->set_value();
				} catch (...) {
					_pPromise->set_exception(std::current_exception());
				}
				_pSemaphore->release();
			}, pPromise, _function, &m_semaphore));
		}

	};

}