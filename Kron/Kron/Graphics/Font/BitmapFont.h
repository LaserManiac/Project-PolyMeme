#pragma once

#include <vector>

#include <SFML\System.hpp>

#include "GlyphLayout.h"
#include "..\Texture\Texture2D.h"

namespace kr {

	class BitmapFont {

	private:
		std::vector<Texture2D> m_texturePages;
		kr::GlyphLayout m_glyphLayout;

	};

}