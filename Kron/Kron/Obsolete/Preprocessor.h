#pragma once

/*
// Meta, used for fixing certain macro bugs.
*/
#define __kr_expand(x) x

/*
// Meta, definitions for left and right parenthesis
*/
#define __kr_lpar (
#define __kr_rpar )

/*
// Meta, used for function calls.
*/
#define __kr_func(_Name, ...) __kr_expand(_Name(__VA_ARGS__))

/*
// Meta, used for combining two values into one
*/
#define __kr_combine_impl(_A, _B) _A ## _B
#define __kr_combine(_A, _B) __kr_expand(__kr_combine_impl(_A, _B))

/*
// Meta, used to surround a value with a prefix and a suffix
*/
#define __kr_surround(_Prefix, _Suffix, _Value) __kr_expand(__kr_combine(__kr_combine(_Prefix, _Value), _Suffix))

/*
// Return a string representation of the value.
*/
#define kr_stringify(_A) #_A

/*
// Macro for counting the number of arguments in a macro variadic argument.
// Supports 1-63 arguments. Does NOT support 0 arguments!
*/
#define __kr_argc(_1,_2,_3,_4,_5,_6,_7,_8,_9,_10,_11,_12,_13,_14,_15,_16,_17,_18,_19,_20,_21,_22,_23,_24,_25,_26,_27,_28,_29,_30,_31,_32,_33,_34,_35,_36,_37,_38,_39,_40,_41,_42,_43,_44,_45,_46,_47,_48,_49,_50,_51,_52,_53,_54,_55,_56,_57,_58,_59,_60,_61,_62,_63,N,...) N
#define __kr_argn(...) __kr_expand(__kr_argc(__VA_ARGS__, 63,62,61,60,59,58,57,56,55,54,53,52,51,50,49,48,47,46,45,44,43,42,41,40,39,38,37,36,35,34,33,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18,17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1,0))
#define kr_varArgCount(...) __kr_argn(__VA_ARGS__)

/*
// Execute given macro on each argument in the variadic argument list
*/
#define __kr_forEach1(_Macro, _V1) _Macro(_V1)
#define __kr_forEach2(_Macro, _V1, _V2) _Macro(_V1) , _Macro(_V2)
#define __kr_forEach3(_Macro, _V1, _V2, _V3) _Macro(_V1) , _Macro(_V2) , _Macro(_V3)
#define __kr_forEach4(_Macro, _V1, _V2, _V3, _V4) _Macro(_V1) , _Macro(_V2) , _Macro(_V3) , _Macro(_V4)
#define __kr_forEach5(_Macro, _V1, _V2, _V3, _V4, _V5) _Macro(_V1) , _Macro(_V2) , _Macro(_V3) , _Macro(_V4) , _Macro(_V5)
#define __kr_forEach6(_Macro, _V1, _V2, _V3, _V4, _V5, _V6) _Macro(_V1) , _Macro(_V2) , _Macro(_V3) , _Macro(_V4) , _Macro(_V5) , _Macro(_V6)
#define __kr_forEach7(_Macro, _V1, _V2, _V3, _V4, _V5, _V6, _V7) _Macro(_V1) , _Macro(_V2) , _Macro(_V3) , _Macro(_V4) , _Macro(_V5) , _Macro(_V6) , _Macro(_V7)
#define __kr_forEach8(_Macro, _V1, _V2, _V3, _V4, _V5, _V6, _V7, _V8) _Macro(_V1) , _Macro(_V2) , _Macro(_V3) , _Macro(_V4) , _Macro(_V5) , _Macro(_V6) , _Macro(_V7) , _Macro(_V8)
#define __kr_forEach9(_Macro, _V1, _V2, _V3, _V4, _V5, _V6, _V7, _V8, _V9) _Macro(_V1) , _Macro(_V2) , _Macro(_V3) , _Macro(_V4) , _Macro(_V5) , _Macro(_V6) , _Macro(_V7) , _Macro(_V8) , _Macro(_V9)
#define __kr_forEach10(_Macro, _V1, _V2, _V3, _V4, _V5, _V6, _V7, _V8, _V9, _V10) _Macro(_V1) , _Macro(_V2) , _Macro(_V3) , _Macro(_V4) , _Macro(_V5) , _Macro(_V6) , _Macro(_V7) , _Macro(_V8) , _Macro(_V9) , _Macro(_V10)
#define __kr_forEach11(_Macro, _V1, _V2, _V3, _V4, _V5, _V6, _V7, _V8, _V9, _V10, _V11) _Macro(_V1) , _Macro(_V2) , _Macro(_V3) , _Macro(_V4) , _Macro(_V5) , _Macro(_V6) , _Macro(_V7) , _Macro(_V8) , _Macro(_V9) , _Macro(_V10) , _Macro(_V11)
#define __kr_forEach12(_Macro, _V1, _V2, _V3, _V4, _V5, _V6, _V7, _V8, _V9, _V10, _V11, _V12) _Macro(_V1) , _Macro(_V2) , _Macro(_V3) , _Macro(_V4) , _Macro(_V5) , _Macro(_V6) , _Macro(_V7) , _Macro(_V8) , _Macro(_V9) , _Macro(_V10) , _Macro(_V11) , _Macro(_V12)
#define __kr_forEach13(_Macro, _V1, _V2, _V3, _V4, _V5, _V6, _V7, _V8, _V9, _V10, _V11, _V12, _V13) _Macro(_V1) , _Macro(_V2) , _Macro(_V3) , _Macro(_V4) , _Macro(_V5) , _Macro(_V6) , _Macro(_V7) , _Macro(_V8) , _Macro(_V9) , _Macro(_V10) , _Macro(_V11) , _Macro(_V12) , _Macro(_V13)
#define __kr_forEach14(_Macro, _V1, _V2, _V3, _V4, _V5, _V6, _V7, _V8, _V9, _V10, _V11, _V12, _V13, _V14) _Macro(_V1) , _Macro(_V2) , _Macro(_V3) , _Macro(_V4) , _Macro(_V5) , _Macro(_V6) , _Macro(_V7) , _Macro(_V8) , _Macro(_V9) , _Macro(_V10) , _Macro(_V11) , _Macro(_V12) , _Macro(_V13) , _Macro(_V14)
#define __kr_forEach15(_Macro, _V1, _V2, _V3, _V4, _V5, _V6, _V7, _V8, _V9, _V10, _V11, _V12, _V13, _V14, _V15) _Macro(_V1) , _Macro(_V2) , _Macro(_V3) , _Macro(_V4) , _Macro(_V5) , _Macro(_V6) , _Macro(_V7) , _Macro(_V8) , _Macro(_V9) , _Macro(_V10) , _Macro(_V11) , _Macro(_V12) , _Macro(_V13) , _Macro(_V14) , _Macro(_V15)
#define __kr_forEach16(_Macro, _V1, _V2, _V3, _V4, _V5, _V6, _V7, _V8, _V9, _V10, _V11, _V12, _V13, _V14, _V15, _V16) _Macro(_V1) , _Macro(_V2) , _Macro(_V3) , _Macro(_V4) , _Macro(_V5) , _Macro(_V6) , _Macro(_V7) , _Macro(_V8) , _Macro(_V9) , _Macro(_V10) , _Macro(_V11) , _Macro(_V12) , _Macro(_V13) , _Macro(_V14) , _Macro(_V15) , _Macro(_V16)
#define kr_forEach(_Macro, ...) __kr_func(__kr_combine(__kr_forEach, kr_varArgCount(__VA_ARGS__)), _Macro, __VA_ARGS__)

/*
// Add given prefix to each argument in the variadic argument list
*/
#define __kr_prefixEach1(_Prefix, _V1) __kr_combine(_Prefix, _V1)
#define __kr_prefixEach2(_Prefix, _V1, _V2) __kr_combine(_Prefix, _V1) , __kr_combine(_Prefix, _V2)
#define __kr_prefixEach3(_Prefix, _V1, _V2, _V3) __kr_combine(_Prefix, _V1) , __kr_combine(_Prefix, _V2) , __kr_combine(_Prefix, _V3)
#define __kr_prefixEach4(_Prefix, _V1, _V2, _V3, _V4) __kr_combine(_Prefix, _V1) , __kr_combine(_Prefix, _V2) , __kr_combine(_Prefix, _V3) , __kr_combine(_Prefix, _V4)
#define __kr_prefixEach5(_Prefix, _V1, _V2, _V3, _V4, _V5) __kr_combine(_Prefix, _V1) , __kr_combine(_Prefix, _V2) , __kr_combine(_Prefix, _V3) , __kr_combine(_Prefix, _V4) , __kr_combine(_Prefix, _V5)
#define __kr_prefixEach6(_Prefix, _V1, _V2, _V3, _V4, _V5, _V6) __kr_combine(_Prefix, _V1) , __kr_combine(_Prefix, _V2) , __kr_combine(_Prefix, _V3) , __kr_combine(_Prefix, _V4) , __kr_combine(_Prefix, _V5) , __kr_combine(_Prefix, _V6)
#define __kr_prefixEach7(_Prefix, _V1, _V2, _V3, _V4, _V5, _V6, _V7) __kr_combine(_Prefix, _V1) , __kr_combine(_Prefix, _V2) , __kr_combine(_Prefix, _V3) , __kr_combine(_Prefix, _V4) , __kr_combine(_Prefix, _V5) , __kr_combine(_Prefix, _V6) , __kr_combine(_Prefix, _V7)
#define __kr_prefixEach8(_Prefix, _V1, _V2, _V3, _V4, _V5, _V6, _V7, _V8) __kr_combine(_Prefix, _V1) , __kr_combine(_Prefix, _V2) , __kr_combine(_Prefix, _V3) , __kr_combine(_Prefix, _V4) , __kr_combine(_Prefix, _V5) , __kr_combine(_Prefix, _V6) , __kr_combine(_Prefix, _V7) , __kr_combine(_Prefix, _V8)
#define __kr_prefixEach9(_Prefix, _V1, _V2, _V3, _V4, _V5, _V6, _V7, _V8, _V9) __kr_combine(_Prefix, _V1) , __kr_combine(_Prefix, _V2) , __kr_combine(_Prefix, _V3) , __kr_combine(_Prefix, _V4) , __kr_combine(_Prefix, _V5) , __kr_combine(_Prefix, _V6) , __kr_combine(_Prefix, _V7) , __kr_combine(_Prefix, _V8) , __kr_combine(_Prefix, _V9)
#define __kr_prefixEach10(_Prefix, _V1, _V2, _V3, _V4, _V5, _V6, _V7, _V8, _V9, _V10) __kr_combine(_Prefix, _V1) , __kr_combine(_Prefix, _V2) , __kr_combine(_Prefix, _V3) , __kr_combine(_Prefix, _V4) , __kr_combine(_Prefix, _V5) , __kr_combine(_Prefix, _V6) , __kr_combine(_Prefix, _V7) , __kr_combine(_Prefix, _V8) , __kr_combine(_Prefix, _V9) , __kr_combine(_Prefix, _V10)
#define __kr_prefixEach11(_Prefix, _V1, _V2, _V3, _V4, _V5, _V6, _V7, _V8, _V9, _V10, _V11) __kr_combine(_Prefix, _V1) , __kr_combine(_Prefix, _V2) , __kr_combine(_Prefix, _V3) , __kr_combine(_Prefix, _V4) , __kr_combine(_Prefix, _V5) , __kr_combine(_Prefix, _V6) , __kr_combine(_Prefix, _V7) , __kr_combine(_Prefix, _V8) , __kr_combine(_Prefix, _V9) , __kr_combine(_Prefix, _V10) , __kr_combine(_Prefix, _V11)
#define __kr_prefixEach12(_Prefix, _V1, _V2, _V3, _V4, _V5, _V6, _V7, _V8, _V9, _V10, _V11, _V12) __kr_combine(_Prefix, _V1) , __kr_combine(_Prefix, _V2) , __kr_combine(_Prefix, _V3) , __kr_combine(_Prefix, _V4) , __kr_combine(_Prefix, _V5) , __kr_combine(_Prefix, _V6) , __kr_combine(_Prefix, _V7) , __kr_combine(_Prefix, _V8) , __kr_combine(_Prefix, _V9) , __kr_combine(_Prefix, _V10) , __kr_combine(_Prefix, _V11) , __kr_combine(_Prefix, _V12)
#define __kr_prefixEach13(_Prefix, _V1, _V2, _V3, _V4, _V5, _V6, _V7, _V8, _V9, _V10, _V11, _V12, _V13) __kr_combine(_Prefix, _V1) , __kr_combine(_Prefix, _V2) , __kr_combine(_Prefix, _V3) , __kr_combine(_Prefix, _V4) , __kr_combine(_Prefix, _V5) , __kr_combine(_Prefix, _V6) , __kr_combine(_Prefix, _V7) , __kr_combine(_Prefix, _V8) , __kr_combine(_Prefix, _V9) , __kr_combine(_Prefix, _V10) , __kr_combine(_Prefix, _V11) , __kr_combine(_Prefix, _V12) , __kr_combine(_Prefix, _V13)
#define __kr_prefixEach14(_Prefix, _V1, _V2, _V3, _V4, _V5, _V6, _V7, _V8, _V9, _V10, _V11, _V12, _V13, _V14) __kr_combine(_Prefix, _V1) , __kr_combine(_Prefix, _V2) , __kr_combine(_Prefix, _V3) , __kr_combine(_Prefix, _V4) , __kr_combine(_Prefix, _V5) , __kr_combine(_Prefix, _V6) , __kr_combine(_Prefix, _V7) , __kr_combine(_Prefix, _V8) , __kr_combine(_Prefix, _V9) , __kr_combine(_Prefix, _V10) , __kr_combine(_Prefix, _V11) , __kr_combine(_Prefix, _V12) , __kr_combine(_Prefix, _V13) , __kr_combine(_Prefix, _V14)
#define __kr_prefixEach15(_Prefix, _V1, _V2, _V3, _V4, _V5, _V6, _V7, _V8, _V9, _V10, _V11, _V12, _V13, _V14, _V15) __kr_combine(_Prefix, _V1) , __kr_combine(_Prefix, _V2) , __kr_combine(_Prefix, _V3) , __kr_combine(_Prefix, _V4) , __kr_combine(_Prefix, _V5) , __kr_combine(_Prefix, _V6) , __kr_combine(_Prefix, _V7) , __kr_combine(_Prefix, _V8) , __kr_combine(_Prefix, _V9) , __kr_combine(_Prefix, _V10) , __kr_combine(_Prefix, _V11) , __kr_combine(_Prefix, _V12) , __kr_combine(_Prefix, _V13) , __kr_combine(_Prefix, _V14) , __kr_combine(_Prefix, _V15)
#define __kr_prefixEach16(_Prefix, _V1, _V2, _V3, _V4, _V5, _V6, _V7, _V8, _V9, _V10, _V11, _V12, _V13, _V14, _V15, _V16) __kr_combine(_Prefix, _V1) , __kr_combine(_Prefix, _V2) , __kr_combine(_Prefix, _V3) , __kr_combine(_Prefix, _V4) , __kr_combine(_Prefix, _V5) , __kr_combine(_Prefix, _V6) , __kr_combine(_Prefix, _V7) , __kr_combine(_Prefix, _V8) , __kr_combine(_Prefix, _V9) , __kr_combine(_Prefix, _V10) , __kr_combine(_Prefix, _V11) , __kr_combine(_Prefix, _V12) , __kr_combine(_Prefix, _V13) , __kr_combine(_Prefix, _V14) , __kr_combine(_Prefix, _V15) , __kr_combine(_Prefix, _V16)
#define kr_prefixEach(_Prefix, ...) __kr_func(__kr_combine(__kr_prefixEach, kr_varArgCount(__VA_ARGS__)), _Prefix, __VA_ARGS__)

/*
// Add given suffix to each argument in the variadic argument list
*/
#define __kr_suffixEach1(_Suffix, _V1) __kr_combine(_V1, _Suffix)
#define __kr_suffixEach2(_Suffix, _V1, _V2) __kr_combine(_V1, _Suffix) , __kr_combine(_V2, _Suffix)
#define __kr_suffixEach3(_Suffix, _V1, _V2, _V3) __kr_combine(_V1, _Suffix) , __kr_combine(_V2, _Suffix) , __kr_combine(_V3, _Suffix)
#define __kr_suffixEach4(_Suffix, _V1, _V2, _V3, _V4) __kr_combine(_V1, _Suffix) , __kr_combine(_V2, _Suffix) , __kr_combine(_V3, _Suffix) , __kr_combine(_V4, _Suffix)
#define __kr_suffixEach5(_Suffix, _V1, _V2, _V3, _V4, _V5) __kr_combine(_V1, _Suffix) , __kr_combine(_V2, _Suffix) , __kr_combine(_V3, _Suffix) , __kr_combine(_V4, _Suffix) , __kr_combine(_V5, _Suffix)
#define __kr_suffixEach6(_Suffix, _V1, _V2, _V3, _V4, _V5, _V6) __kr_combine(_V1, _Suffix) , __kr_combine(_V2, _Suffix) , __kr_combine(_V3, _Suffix) , __kr_combine(_V4, _Suffix) , __kr_combine(_V5, _Suffix) , __kr_combine(_V6, _Suffix)
#define __kr_suffixEach7(_Suffix, _V1, _V2, _V3, _V4, _V5, _V6, _V7) __kr_combine(_V1, _Suffix) , __kr_combine(_V2, _Suffix) , __kr_combine(_V3, _Suffix) , __kr_combine(_V4, _Suffix) , __kr_combine(_V5, _Suffix) , __kr_combine(_V6, _Suffix) , __kr_combine(_V7, _Suffix)
#define __kr_suffixEach8(_Suffix, _V1, _V2, _V3, _V4, _V5, _V6, _V7, _V8) __kr_combine(_V1, _Suffix) , __kr_combine(_V2, _Suffix) , __kr_combine(_V3, _Suffix) , __kr_combine(_V4, _Suffix) , __kr_combine(_V5, _Suffix) , __kr_combine(_V6, _Suffix) , __kr_combine(_V7, _Suffix) , __kr_combine(_V8, _Suffix)
#define __kr_suffixEach9(_Suffix, _V1, _V2, _V3, _V4, _V5, _V6, _V7, _V8, _V9) __kr_combine(_V1, _Suffix) , __kr_combine(_V2, _Suffix) , __kr_combine(_V3, _Suffix) , __kr_combine(_V4, _Suffix) , __kr_combine(_V5, _Suffix) , __kr_combine(_V6, _Suffix) , __kr_combine(_V7, _Suffix) , __kr_combine(_V8, _Suffix) , __kr_combine(_V9, _Suffix)
#define __kr_suffixEach10(_Suffix, _V1, _V2, _V3, _V4, _V5, _V6, _V7, _V8, _V9, _V10) __kr_combine(_V1, _Suffix) , __kr_combine(_V2, _Suffix) , __kr_combine(_V3, _Suffix) , __kr_combine(_V4, _Suffix) , __kr_combine(_V5, _Suffix) , __kr_combine(_V6, _Suffix) , __kr_combine(_V7, _Suffix) , __kr_combine(_V8, _Suffix) , __kr_combine(_V9, _Suffix) , __kr_combine(_V10, _Suffix)
#define __kr_suffixEach11(_Suffix, _V1, _V2, _V3, _V4, _V5, _V6, _V7, _V8, _V9, _V10, _V11) __kr_combine(_V1, _Suffix) , __kr_combine(_V2, _Suffix) , __kr_combine(_V3, _Suffix) , __kr_combine(_V4, _Suffix) , __kr_combine(_V5, _Suffix) , __kr_combine(_V6, _Suffix) , __kr_combine(_V7, _Suffix) , __kr_combine(_V8, _Suffix) , __kr_combine(_V9, _Suffix) , __kr_combine(_V10, _Suffix) , __kr_combine(_V11, _Suffix)
#define __kr_suffixEach12(_Suffix, _V1, _V2, _V3, _V4, _V5, _V6, _V7, _V8, _V9, _V10, _V11, _V12) __kr_combine(_V1, _Suffix) , __kr_combine(_V2, _Suffix) , __kr_combine(_V3, _Suffix) , __kr_combine(_V4, _Suffix) , __kr_combine(_V5, _Suffix) , __kr_combine(_V6, _Suffix) , __kr_combine(_V7, _Suffix) , __kr_combine(_V8, _Suffix) , __kr_combine(_V9, _Suffix) , __kr_combine(_V10, _Suffix) , __kr_combine(_V11, _Suffix) , __kr_combine(_V12, _Suffix)
#define __kr_suffixEach13(_Suffix, _V1, _V2, _V3, _V4, _V5, _V6, _V7, _V8, _V9, _V10, _V11, _V12, _V13) __kr_combine(_V1, _Suffix) , __kr_combine(_V2, _Suffix) , __kr_combine(_V3, _Suffix) , __kr_combine(_V4, _Suffix) , __kr_combine(_V5, _Suffix) , __kr_combine(_V6, _Suffix) , __kr_combine(_V7, _Suffix) , __kr_combine(_V8, _Suffix) , __kr_combine(_V9, _Suffix) , __kr_combine(_V10, _Suffix) , __kr_combine(_V11, _Suffix) , __kr_combine(_V12, _Suffix) , __kr_combine(_V13, _Suffix)
#define __kr_suffixEach14(_Suffix, _V1, _V2, _V3, _V4, _V5, _V6, _V7, _V8, _V9, _V10, _V11, _V12, _V13, _V14) __kr_combine(_V1, _Suffix) , __kr_combine(_V2, _Suffix) , __kr_combine(_V3, _Suffix) , __kr_combine(_V4, _Suffix) , __kr_combine(_V5, _Suffix) , __kr_combine(_V6, _Suffix) , __kr_combine(_V7, _Suffix) , __kr_combine(_V8, _Suffix) , __kr_combine(_V9, _Suffix) , __kr_combine(_V10, _Suffix) , __kr_combine(_V11, _Suffix) , __kr_combine(_V12, _Suffix) , __kr_combine(_V13, _Suffix) , __kr_combine(_V14, _Suffix)
#define __kr_suffixEach15(_Suffix, _V1, _V2, _V3, _V4, _V5, _V6, _V7, _V8, _V9, _V10, _V11, _V12, _V13, _V14, _V15) __kr_combine(_V1, _Suffix) , __kr_combine(_V2, _Suffix) , __kr_combine(_V3, _Suffix) , __kr_combine(_V4, _Suffix) , __kr_combine(_V5, _Suffix) , __kr_combine(_V6, _Suffix) , __kr_combine(_V7, _Suffix) , __kr_combine(_V8, _Suffix) , __kr_combine(_V9, _Suffix) , __kr_combine(_V10, _Suffix) , __kr_combine(_V11, _Suffix) , __kr_combine(_V12, _Suffix) , __kr_combine(_V13, _Suffix) , __kr_combine(_V14, _Suffix) , __kr_combine(_V15, _Suffix)
#define __kr_suffixEach16(_Suffix, _V1, _V2, _V3, _V4, _V5, _V6, _V7, _V8, _V9, _V10, _V11, _V12, _V13, _V14, _V15, _V16) __kr_combine(_V1, _Suffix) , __kr_combine(_V2, _Suffix) , __kr_combine(_V3, _Suffix) , __kr_combine(_V4, _Suffix) , __kr_combine(_V5, _Suffix) , __kr_combine(_V6, _Suffix) , __kr_combine(_V7, _Suffix) , __kr_combine(_V8, _Suffix) , __kr_combine(_V9, _Suffix) , __kr_combine(_V10, _Suffix) , __kr_combine(_V11, _Suffix) , __kr_combine(_V12, _Suffix) , __kr_combine(_V13, _Suffix) , __kr_combine(_V14, _Suffix) , __kr_combine(_V15, _Suffix) , __kr_combine(_V16, _Suffix)
#define kr_suffixEach(_Suffix, ...) __kr_func(__kr_combine(__kr_suffixEach, kr_varArgCount(__VA_ARGS__)), _Suffix, __VA_ARGS__)

/*
// Surround each argument in the variadic argument list
*/
#define __kr_surroundEach1(_Prefix, _Suffix, _V1) __kr_surround(_Prefix, _Suffix, _V1)
#define __kr_surroundEach2(_Prefix, _Suffix, _V1, _V2) __kr_surround(_Prefix, _Suffix, _V1) , __kr_surround(_Prefix, _Suffix, _V2)
#define __kr_surroundEach3(_Prefix, _Suffix, _V1, _V2, _V3) __kr_surround(_Prefix, _Suffix, _V1) , __kr_surround(_Prefix, _Suffix, _V2) , __kr_surround(_Prefix, _Suffix, _V3)
#define __kr_surroundEach4(_Prefix, _Suffix, _V1, _V2, _V3, _V4) __kr_surround(_Prefix, _Suffix, _V1) , __kr_surround(_Prefix, _Suffix, _V2) , __kr_surround(_Prefix, _Suffix, _V3) , __kr_surround(_Prefix, _Suffix, _V4)
#define __kr_surroundEach5(_Prefix, _Suffix, _V1, _V2, _V3, _V4, _V5) __kr_surround(_Prefix, _Suffix, _V1) , __kr_surround(_Prefix, _Suffix, _V2) , __kr_surround(_Prefix, _Suffix, _V3) , __kr_surround(_Prefix, _Suffix, _V4) , __kr_surround(_Prefix, _Suffix, _V5)
#define __kr_surroundEach6(_Prefix, _Suffix, _V1, _V2, _V3, _V4, _V5, _V6) __kr_surround(_Prefix, _Suffix, _V1) , __kr_surround(_Prefix, _Suffix, _V2) , __kr_surround(_Prefix, _Suffix, _V3) , __kr_surround(_Prefix, _Suffix, _V4) , __kr_surround(_Prefix, _Suffix, _V5) , __kr_surround(_Prefix, _Suffix, _V6)
#define __kr_surroundEach7(_Prefix, _Suffix, _V1, _V2, _V3, _V4, _V5, _V6, _V7) __kr_surround(_Prefix, _Suffix, _V1) , __kr_surround(_Prefix, _Suffix, _V2) , __kr_surround(_Prefix, _Suffix, _V3) , __kr_surround(_Prefix, _Suffix, _V4) , __kr_surround(_Prefix, _Suffix, _V5) , __kr_surround(_Prefix, _Suffix, _V6) , __kr_surround(_Prefix, _Suffix, _V7)
#define __kr_surroundEach8(_Prefix, _Suffix, _V1, _V2, _V3, _V4, _V5, _V6, _V7, _V8) __kr_surround(_Prefix, _Suffix, _V1) , __kr_surround(_Prefix, _Suffix, _V2) , __kr_surround(_Prefix, _Suffix, _V3) , __kr_surround(_Prefix, _Suffix, _V4) , __kr_surround(_Prefix, _Suffix, _V5) , __kr_surround(_Prefix, _Suffix, _V6) , __kr_surround(_Prefix, _Suffix, _V7) , __kr_surround(_Prefix, _Suffix, _V8)
#define __kr_surroundEach9(_Prefix, _Suffix, _V1, _V2, _V3, _V4, _V5, _V6, _V7, _V8, _V9) __kr_surround(_Prefix, _Suffix, _V1) , __kr_surround(_Prefix, _Suffix, _V2) , __kr_surround(_Prefix, _Suffix, _V3) , __kr_surround(_Prefix, _Suffix, _V4) , __kr_surround(_Prefix, _Suffix, _V5) , __kr_surround(_Prefix, _Suffix, _V6) , __kr_surround(_Prefix, _Suffix, _V7) , __kr_surround(_Prefix, _Suffix, _V8) , __kr_surround(_Prefix, _Suffix, _V9)
#define __kr_surroundEach10(_Prefix, _Suffix, _V1, _V2, _V3, _V4, _V5, _V6, _V7, _V8, _V9, _V10) __kr_surround(_Prefix, _Suffix, _V1) , __kr_surround(_Prefix, _Suffix, _V2) , __kr_surround(_Prefix, _Suffix, _V3) , __kr_surround(_Prefix, _Suffix, _V4) , __kr_surround(_Prefix, _Suffix, _V5) , __kr_surround(_Prefix, _Suffix, _V6) , __kr_surround(_Prefix, _Suffix, _V7) , __kr_surround(_Prefix, _Suffix, _V8) , __kr_surround(_Prefix, _Suffix, _V9) , __kr_surround(_Prefix, _Suffix, _V10)
#define __kr_surroundEach11(_Prefix, _Suffix, _V1, _V2, _V3, _V4, _V5, _V6, _V7, _V8, _V9, _V10, _V11) __kr_surround(_Prefix, _Suffix, _V1) , __kr_surround(_Prefix, _Suffix, _V2) , __kr_surround(_Prefix, _Suffix, _V3) , __kr_surround(_Prefix, _Suffix, _V4) , __kr_surround(_Prefix, _Suffix, _V5) , __kr_surround(_Prefix, _Suffix, _V6) , __kr_surround(_Prefix, _Suffix, _V7) , __kr_surround(_Prefix, _Suffix, _V8) , __kr_surround(_Prefix, _Suffix, _V9) , __kr_surround(_Prefix, _Suffix, _V10) , __kr_surround(_Prefix, _Suffix, _V11)
#define __kr_surroundEach12(_Prefix, _Suffix, _V1, _V2, _V3, _V4, _V5, _V6, _V7, _V8, _V9, _V10, _V11, _V12) __kr_surround(_Prefix, _Suffix, _V1) , __kr_surround(_Prefix, _Suffix, _V2) , __kr_surround(_Prefix, _Suffix, _V3) , __kr_surround(_Prefix, _Suffix, _V4) , __kr_surround(_Prefix, _Suffix, _V5) , __kr_surround(_Prefix, _Suffix, _V6) , __kr_surround(_Prefix, _Suffix, _V7) , __kr_surround(_Prefix, _Suffix, _V8) , __kr_surround(_Prefix, _Suffix, _V9) , __kr_surround(_Prefix, _Suffix, _V10) , __kr_surround(_Prefix, _Suffix, _V11) , __kr_surround(_Prefix, _Suffix, _V12)
#define __kr_surroundEach13(_Prefix, _Suffix, _V1, _V2, _V3, _V4, _V5, _V6, _V7, _V8, _V9, _V10, _V11, _V12, _V13) __kr_surround(_Prefix, _Suffix, _V1) , __kr_surround(_Prefix, _Suffix, _V2) , __kr_surround(_Prefix, _Suffix, _V3) , __kr_surround(_Prefix, _Suffix, _V4) , __kr_surround(_Prefix, _Suffix, _V5) , __kr_surround(_Prefix, _Suffix, _V6) , __kr_surround(_Prefix, _Suffix, _V7) , __kr_surround(_Prefix, _Suffix, _V8) , __kr_surround(_Prefix, _Suffix, _V9) , __kr_surround(_Prefix, _Suffix, _V10) , __kr_surround(_Prefix, _Suffix, _V11) , __kr_surround(_Prefix, _Suffix, _V12) , __kr_surround(_Prefix, _Suffix, _V13)
#define __kr_surroundEach14(_Prefix, _Suffix, _V1, _V2, _V3, _V4, _V5, _V6, _V7, _V8, _V9, _V10, _V11, _V12, _V13, _V14) __kr_surround(_Prefix, _Suffix, _V1) , __kr_surround(_Prefix, _Suffix, _V2) , __kr_surround(_Prefix, _Suffix, _V3) , __kr_surround(_Prefix, _Suffix, _V4) , __kr_surround(_Prefix, _Suffix, _V5) , __kr_surround(_Prefix, _Suffix, _V6) , __kr_surround(_Prefix, _Suffix, _V7) , __kr_surround(_Prefix, _Suffix, _V8) , __kr_surround(_Prefix, _Suffix, _V9) , __kr_surround(_Prefix, _Suffix, _V10) , __kr_surround(_Prefix, _Suffix, _V11) , __kr_surround(_Prefix, _Suffix, _V12) , __kr_surround(_Prefix, _Suffix, _V13) , __kr_surround(_Prefix, _Suffix, _V14)
#define __kr_surroundEach15(_Prefix, _Suffix, _V1, _V2, _V3, _V4, _V5, _V6, _V7, _V8, _V9, _V10, _V11, _V12, _V13, _V14, _V15) __kr_surround(_Prefix, _Suffix, _V1) , __kr_surround(_Prefix, _Suffix, _V2) , __kr_surround(_Prefix, _Suffix, _V3) , __kr_surround(_Prefix, _Suffix, _V4) , __kr_surround(_Prefix, _Suffix, _V5) , __kr_surround(_Prefix, _Suffix, _V6) , __kr_surround(_Prefix, _Suffix, _V7) , __kr_surround(_Prefix, _Suffix, _V8) , __kr_surround(_Prefix, _Suffix, _V9) , __kr_surround(_Prefix, _Suffix, _V10) , __kr_surround(_Prefix, _Suffix, _V11) , __kr_surround(_Prefix, _Suffix, _V12) , __kr_surround(_Prefix, _Suffix, _V13) , __kr_surround(_Prefix, _Suffix, _V14) , __kr_surround(_Prefix, _Suffix, _V15)
#define __kr_surroundEach16(_Prefix, _Suffix, _V1, _V2, _V3, _V4, _V5, _V6, _V7, _V8, _V9, _V10, _V11, _V12, _V13, _V14, _V15, _V16) __kr_surround(_Prefix, _Suffix, _V1) , __kr_surround(_Prefix, _Suffix, _V2) , __kr_surround(_Prefix, _Suffix, _V3) , __kr_surround(_Prefix, _Suffix, _V4) , __kr_surround(_Prefix, _Suffix, _V5) , __kr_surround(_Prefix, _Suffix, _V6) , __kr_surround(_Prefix, _Suffix, _V7) , __kr_surround(_Prefix, _Suffix, _V8) , __kr_surround(_Prefix, _Suffix, _V9) , __kr_surround(_Prefix, _Suffix, _V10) , __kr_surround(_Prefix, _Suffix, _V11) , __kr_surround(_Prefix, _Suffix, _V12) , __kr_surround(_Prefix, _Suffix, _V13) , __kr_surround(_Prefix, _Suffix, _V14) , __kr_surround(_Prefix, _Suffix, _V15) , __kr_surround(_Prefix, _Suffix, _V16)
#define kr_surroundEach(_Prefix, _Suffix, ...) __kr_func(__kr_combine(__kr_surroundEach, kr_varArgCount(__VA_ARGS__)), _Prefix, _Suffix, __VA_ARGS__)
