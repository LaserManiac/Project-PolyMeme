#pragma once

#include <GMTL/Vec.h>
#include <GMTL/VecOps.h>
#include <GMTL/Point.h>
#include <GMTL/Matrix.h>
#include <GMTL/MatrixOps.h>
#include <GMTL/Math.h>

namespace kr {

	// ==========================================================
	//  A transform class containing a transform matrix.
	//  Supports translation, rotation, and scaling in 3D space.
	// ==========================================================
	class Transform3D : public gmtl::Matrix44f {
	public:
		Transform3D() : gmtl::Matrix44f() {}
		Transform3D(const gmtl::Matrix44f &&_mat) : gmtl::Matrix44f(_mat) {}

		Transform3D& setToTranslation(const gmtl::Point3f &_position);
		Transform3D& setToRotationX(float _deg);
		Transform3D& setToRotationY(float _deg);
		Transform3D& setToRotationZ(float _deg);
		Transform3D& setToRotation(const gmtl::Vec3f &_axis, float _deg);
		Transform3D& setToScale(float _scale);

		Transform3D& translate(const gmtl::Point3f &_position);
		Transform3D& rotateX(float _deg);
		Transform3D& rotateY(float _deg);
		Transform3D& rotateZ(float _deg);
		Transform3D& rotate(const gmtl::Vec3f &_axis, float _deg);
		Transform3D& scale(float _scale);

		Transform3D& setToBasis(const gmtl::Vec3f &_xAxis, const gmtl::Vec3f &_yAxis, const gmtl::Vec3f &_zAxis, const gmtl::Point3f &_origin);
		Transform3D& setToView(const gmtl::Point3f &_position, const gmtl::Vec3f &_direction, const gmtl::Vec3f &_up);

		static Transform3D Translation(const gmtl::Point3f &_position);
		static Transform3D RotationX(float _deg);
		static Transform3D RotationY(float _deg);
		static Transform3D RotationZ(float _deg);
		static Transform3D Rotation(const gmtl::Vec3f &_axis, float _deg);
		static Transform3D Scale(float _scale);

		static Transform3D Basis(const gmtl::Vec3f &_xAxis, const gmtl::Vec3f &_yAxis, const gmtl::Vec3f &_zAxis, const gmtl::Point3f &_origin);
		static Transform3D View(const gmtl::Point3f &_position, const gmtl::Vec3f &_direction, const gmtl::Vec3f &_up);

	};

}