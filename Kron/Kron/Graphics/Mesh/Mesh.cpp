#include "Mesh.h"

namespace kr {
	namespace gfx {

		Mesh::Mesh(MeshData* _pData)
			: m_pData(_pData) {

			const IndexData &indexData = m_pData->getIndexData();
			m_indexBuffer.bind();
			m_indexBuffer.OglBuffer::uploadData(indexData.getData(), GL_STATIC_DRAW);

			m_vertexArray.linkElementArray(m_indexBuffer);

			for (const MeshBuffer &meshBuffer : m_pData->getMeshBuffers()) {

				ArrayBuffer arrayBuffer;
				arrayBuffer.bind();
				arrayBuffer.OglBuffer::uploadData(meshBuffer.getBufferData().getData(), meshBuffer.getBufferLayout().getUsage());

				m_vertexArray.linkArrayBuffer(arrayBuffer, meshBuffer.getBufferLayout());
				
				m_arrayBuffers.emplace_back(std::move(arrayBuffer));

			}

		}

		const MeshData* Mesh::getMeshData() const { return m_pData; }

		const VertexArray& Mesh::getVerteyArray() const { return m_vertexArray; }

		const ElementBuffer& Mesh::getIndexBuffer() const { return m_indexBuffer; }

		const std::vector<ArrayBuffer>& Mesh::getArrayBuffers() const { return m_arrayBuffers; }

		void Mesh::draw(const Mesh &_mesh) {
			_mesh.getVerteyArray().bind();
			const IndexData &indexData = _mesh.getMeshData()->getIndexData();
			kr_glCall(glDrawElements(_mesh.getMeshData()->getPrimitiveType(), indexData.getIndexNumber(), indexData.getIndexType(), NULL));
		}
	}
}