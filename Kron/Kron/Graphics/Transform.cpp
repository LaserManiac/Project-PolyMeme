#include "Transform.h"

kr::Transform3D& kr::Transform3D::setToTranslation(const gmtl::Point3f &_position) {
	gmtl::identity(*this);
	mState = XformState::TRANS;
	gmtl::Matrix44f& self = *this;

	self(0, 3) = _position[0];
	self(1, 3) = _position[1];
	self(2, 3) = _position[2];

	return *this;
}

kr::Transform3D& kr::Transform3D::setToRotationX(float _deg) {
	gmtl::identity(*this);
	mState = XformState::ORTHOGONAL;
	gmtl::Matrix44f& self = *this;

	float rad = gmtl::Math::deg2Rad(_deg);
	float cos = gmtl::Math::cos(rad);
	float sin = gmtl::Math::sin(rad);

	self(1, 1) = cos;
	self(2, 1) = sin;
	self(1, 2) = -sin;
	self(2, 2) = cos;

	return *this;
}

kr::Transform3D& kr::Transform3D::setToRotationY(float _deg) {
	gmtl::identity(*this);
	mState = XformState::ORTHOGONAL;
	gmtl::Matrix44f& self = *this;

	float rad = gmtl::Math::deg2Rad(_deg);
	float cos = gmtl::Math::cos(rad);
	float sin = gmtl::Math::sin(rad);

	self(0, 0) = cos;
	self(2, 0) = -sin;
	self(0, 2) = sin;
	self(2, 2) = cos;

	return *this;
}

kr::Transform3D& kr::Transform3D::setToRotationZ(float _deg) {
	gmtl::identity(*this);
	mState = XformState::ORTHOGONAL;
	gmtl::Matrix44f& self = *this;

	float rad = gmtl::Math::deg2Rad(_deg);
	float cos = gmtl::Math::cos(rad);
	float sin = gmtl::Math::sin(rad);

	self(0, 0) = cos;
	self(1, 0) = sin;
	self(0, 1) = -sin;
	self(1, 1) = cos;

	return *this;
}

kr::Transform3D& kr::Transform3D::setToRotation(const gmtl::Vec3f &_axis, float _deg) {
	mState = XformState::FULL;
	gmtl::Matrix44f& self = *this;

	float rad = gmtl::Math::deg2Rad(_deg);
	float sin = gmtl::Math::sin(rad);
	float cos = gmtl::Math::cos(rad);
	float oneMinusCos = 1 - cos;
	float x = _axis[0];
	float y = _axis[1];
	float z = _axis[2];

	self(0, 0) = cos + x * x * oneMinusCos;
	self(1, 0) = y * x * oneMinusCos + z * sin;
	self(2, 0) = z * x * oneMinusCos - y * sin;
	self(3, 0) = 0;

	self(0, 1) = x * y * oneMinusCos - z * sin;
	self(1, 1) = cos + y * y * oneMinusCos;
	self(2, 1) = z * y * oneMinusCos + x * sin;
	self(3, 1) = 0;

	self(0, 2) = x * z * oneMinusCos + x * sin;
	self(1, 2) = y * z * oneMinusCos - y * sin;
	self(2, 2) = cos + z * z * oneMinusCos;
	self(3, 2) = 0;

	self(0, 3) = 0;
	self(1, 3) = 0;
	self(2, 3) = 0;
	self(3, 3) = 1;

	return *this;
}

kr::Transform3D& kr::Transform3D::setToScale(float _scale) {
	gmtl::identity(*this);
	mState = XformState::AFFINE;
	gmtl::Matrix44f& self = *this;

	self(0, 0) = _scale;
	self(1, 1) = _scale;
	self(2, 2) = _scale;

	return *this;
}



kr::Transform3D& kr::Transform3D::translate(const gmtl::Point3f &_position) {
	(*this) *= Transform3D().setToTranslation(_position);
	return *this;
}

kr::Transform3D& kr::Transform3D::rotateX(float _deg) {
	(*this) *= Transform3D().setToRotationX(_deg);
	return *this;
}

kr::Transform3D& kr::Transform3D::rotateY(float _deg) {
	(*this) *= Transform3D().setToRotationY(_deg);
	return *this;
}

kr::Transform3D& kr::Transform3D::rotateZ(float _deg) {
	(*this) *= Transform3D().setToRotationZ(_deg);
	return *this;
}

kr::Transform3D& kr::Transform3D::rotate(const gmtl::Vec3f &_axis, float _deg) {
	(*this) *= Transform3D().setToRotation(_axis, _deg);
	return *this;
}

kr::Transform3D& kr::Transform3D::scale(float _scale) {
	(*this) *= Transform3D().setToScale(_scale);
	return *this;
}

kr::Transform3D& kr::Transform3D::setToBasis(const gmtl::Vec3f &_xAxis, const gmtl::Vec3f &_yAxis, const gmtl::Vec3f &_zAxis, const gmtl::Point3f &_origin) {
	mState = XformState::ORTHOGONAL;
	gmtl::Matrix44f& self = *this;

	self(0, 0) = _xAxis[0];
	self(1, 0) = _yAxis[0];
	self(2, 0) = _zAxis[0];
	self(3, 0) = 0;

	self(0, 1) = _xAxis[1];
	self(1, 1) = _yAxis[1];
	self(2, 1) = _zAxis[1];
	self(3, 1) = 0;

	self(0, 2) = _xAxis[2];
	self(1, 2) = _yAxis[2];
	self(2, 2) = _zAxis[2];
	self(3, 1) = 0;

	self(0, 3) = -gmtl::dot(_xAxis, _origin);
	self(1, 3) = -gmtl::dot(_yAxis, _origin);
	self(2, 3) = -gmtl::dot(_zAxis, _origin);
	self(3, 3) = 1;

	return *this;
}

kr::Transform3D& kr::Transform3D::setToView(const gmtl::Point3f &_position, const gmtl::Vec3f &_direction, const gmtl::Vec3f &_up) {
	
	// idfk
	gmtl::Vec3f direction = -_direction;

	gmtl::Vec3f right;
	gmtl::cross(right, _up, _direction);
	gmtl::normalize(right);

	gmtl::Vec3f up;
	gmtl::cross(up, _direction, right);
	gmtl::normalize(up);

	setToBasis(right, up, direction, _position);

	return *this;
}



kr::Transform3D kr::Transform3D::Translation(const gmtl::Point3f &_position) {
	return Transform3D().setToTranslation(_position);
}

kr::Transform3D kr::Transform3D::RotationX(float _deg) {
	return Transform3D().setToRotationX(_deg);
}

kr::Transform3D kr::Transform3D::RotationY(float _deg) {
	return Transform3D().setToRotationY(_deg);
}

kr::Transform3D kr::Transform3D::RotationZ(float _deg) {
	return Transform3D().setToRotationZ(_deg);
}

kr::Transform3D kr::Transform3D::Rotation(const gmtl::Vec3f &_axis, float _deg) {
	return Transform3D().setToRotation(_axis, _deg);
}

kr::Transform3D kr::Transform3D::Scale(float _scale) {
	return Transform3D().setToScale(_scale);
}

kr::Transform3D kr::Transform3D::Basis(const gmtl::Vec3f &_xAxis, const gmtl::Vec3f &_yAxis, const gmtl::Vec3f &_zAxis, const gmtl::Point3f &_origin) {
	return Transform3D().setToBasis(_xAxis, _yAxis, _zAxis, _origin);
}

kr::Transform3D kr::Transform3D::View(const gmtl::Point3f &_position, const gmtl::Vec3f &_direction, const gmtl::Vec3f &_up) {
	return Transform3D().setToView(_position, _direction, _up);
}
