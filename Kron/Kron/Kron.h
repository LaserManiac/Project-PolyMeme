#pragma once

// =====================
//  Kron core includes.
// =====================

#include "Core\Debug.h"
#include "Core\Class.h"
#include "Core\Types.h"
#include "Core\String.h"
#include "Core\Math.h"
#include "Core\Exception.h"
#include "Core\Logging.h"