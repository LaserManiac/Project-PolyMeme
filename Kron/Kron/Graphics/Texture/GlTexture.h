#pragma once

#include <GL\glew.h>
#include <SFML\OpenGL.hpp>

#include <iostream>

namespace kr {

	template<typename GLenum Type>
	class GlTexture {
	
	private:
		GLuint m_handle;
		GLenum m_format;

	public:
		GlTexture(const GlTexture &&_other) = delete;
		GlTexture& operator=(const GlTexture &&_other) = delete;

		GlTexture(GLenum _format)
			: m_format(_format) {

			glGenTextures(1, &m_handle);
		}

		~GlTexture() {
			glDeleteTextures(1, &m_handle);
		}

		GLuint getHandle() const {
			return m_handle;
		}

		GLenum getFormat() const {
			return m_format;
		}

		void setWrapping(GLenum s, GLenum t, GLenum r) {
			glTexParameteri(Type, GL_TEXTURE_WRAP_S, s);
			glTexParameteri(Type, GL_TEXTURE_WRAP_T, t);
			glTexParameteri(Type, GL_TEXTURE_WRAP_R, r);
		}

		void setFiltering(GLenum min, GLenum mag) {
			glTexParameteri(Type, GL_TEXTURE_MIN_FILTER, min);
			glTexParameteri(Type, GL_TEXTURE_MAG_FILTER, mag);
		}

		void generateMipMaps() {
			glGenerateMipmap(Type);
		}

		void bind() {
			glBindTexture(Type, m_handle);
		}

	};

}