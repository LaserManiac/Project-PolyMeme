#pragma once

#include <iostream>
#include <functional>

#include <gl\glew.h>
#include <SFML\OpenGL.hpp>
#include <SFML\Graphics\Color.hpp>

#include "Core\Types.h"
#include "Core\String.h"
#include "Core\Class.h"
#include "Core\Debug.h"

using GLhandle = typename GLuint;

namespace kr {

	// Color type
	using Color = typename sf::Color;

	// ============================================
	//  A simple class used for initializing glew.
	// ============================================
	class Graphics {
	public:
		static kr::Bool init();
	};



	// ===================
	//  A graphics error.
	// ===================
	class GfxError {
	public:
		const GLenum m_error;
		const kr::i32 m_line;
		const kr::String m_file;
		const kr::String m_code;

	public:
		GfxError(GLenum _error, kr::i32 _line, const kr::String &_file, const kr::String &_code)
			: m_error(_error), m_line(_line), m_file(_file), m_code(_code) {};

	};

	// Graphics error to output stream.
	std::ostream& operator<<(std::ostream &_out, const GfxError &_error);



	// =================================================
	//  A static class used for handling OpenGL errors.
	// =================================================
	class GfxErrors : public NonConstructible {
	public:
		using ErrorHandler = typename std::function<void(const GfxError&)>;

	private:
		static ErrorHandler s_errorHandler;

	public:
		// A default error handler that prints out the error to the console.
		static void defaultErrorHandler(const GfxError &_error);
		// Sets the current error handler.
		static void setErrorHandler(ErrorHandler _handler);

		// Returns a string representation of the provided OpenGL error constant.
		// GL_UNKNOWN_ERROR is returned if the error code is unknown.
		static const kr::String decodeError(GLenum _error);

		// Clears all OpenGL errors from the error buffer.
		static void clearErrors();
		// Checks for any errors and handles each one.
		static void handleErrors(const kr::String &_file, kr::i32 _line, const kr::String &_code);
	};

}

// An error handling macro that should wrap any OpenGL calls.
// If debug mode is enabled, the call is wrapped in error checking functions.
#if kr_isDebugMode == true
	#define kr_glCall(x)\
		{\
		kr::GfxErrors::clearErrors();\
		x;\
		kr::GfxErrors::handleErrors(kr::str(__FILE__), __LINE__, kr::str(#x));\
		}
#else
	#define kr_glCall(x) x
#endif
