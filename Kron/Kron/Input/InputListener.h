#pragma once

#include <functional>
#include <vector>

#include <SFML\Window.hpp>

#include "Core\Types.h"
#include "Input\Keys.h"

namespace kr {

	using MouseButton = typename sf::Mouse::Button;

	// =======================================
	//  An interface used for handling input.
	// =======================================
	class InputListener {
	public:
		
		// A key has been pressed.
		// _key : Keycode of the key.
		// _ctrl : Whether the ctrl key is being held down.
		// _alt : Whether the alt key is being held down.
		// _shift : Whether the shift key is being held down.
		// Return : Whether the event has been handled successfully.
		virtual kr::Bool keyPressed(Keycode _key, kr::Bool _ctrl, kr::Bool _alt, kr::Bool _shift) { return false; }
		
		// A key has been released.
		// _key : Keycode of the key.
		// _ctrl : Whether the ctrl key is being held down.
		// _alt : Whether the alt key is being held down.
		// _shift : Whether the shift key is being held down.
		// Return : Whether the event has been handled successfully.
		virtual kr::Bool keyReleased(Keycode _key, kr::Bool _ctrl, kr::Bool _alt, kr::Bool _shift) { return false; }
		
		// A mouse button has been pressed.
		// _button : Button that has been pressed.
		// _x : Cursor X position in the window.
		// _y : Cursor Y position in the window.
		// Return : Whether the event has been handled successfully.
		virtual kr::Bool mouseButtonPressed(MouseButton _button, kr::u16 _x, kr::u16 _y) { return false; }
		
		// A mouse button has been released.
		// _button : Button that has been released.
		// _x : Cursor X position in the window.
		// _y : Cursor Y position in the window.
		// Return : Whether the event has been handled successfully.
		virtual kr::Bool mouseButtonReleased(MouseButton _button, kr::u16 _x, kr::u16 _y) { return false; }
		
		// The mouse wheel has been scrolled.
		// _amount : The amount the mouse wheel has been scrolled: 1 or -1.
		// Return : Whether the event has been handled successfully.
		virtual kr::Bool mouseWheelScrolled(kr::u8 _amount) { return false; }
		
		// The mouse cursor has moved.
		// _x : Cursor X position in the window.
		// _y : Cursor Y position in the window.
		// Return : Whether the event has been handled successfully.
		virtual kr::Bool mouseMoved(kr::u16 _x, kr::u16 _y) { return false; }
		
		// A text character has been typed.
		// _character : The character that has been typed.
		// Return : Whether the event has been handled successfully.
		virtual kr::Bool textTyped(kr::u32 _character) { return false; }

	};



	// =================================================================================================================
	//  An input listener that forwards all events to a list of other listeners.
	//  If a listener returns true, the input is considered to be processed and is not forwarded to any more listeners.
	// =================================================================================================================
	class InputMultiplexer : public InputListener {
	private:
		std::vector<InputListener*> m_listeners;
		std::vector<InputListener*> m_managedListeners;

	public:
		// Destructor, deletes all managed listeners.
		~InputMultiplexer();

		// Add a listener to the multiplexer.
		// _pListener : The listener that is to be added.
		// _managed : Whether the multiplexer is responsible for deleting the listener.
		void addListener(InputListener* _pListener, kr::Bool _managed = true);

		// Remove a listener from the multiplexer.
		void removeListener(InputListener* _pListener);

		kr::Bool keyPressed(Keycode _key, kr::Bool _ctrl, kr::Bool _alt, kr::Bool _shift);
		kr::Bool keyReleased(Keycode _key, kr::Bool _ctrl, kr::Bool _alt, kr::Bool _shift);
		kr::Bool mouseButtonPressed(MouseButton _button, kr::u16 _x, kr::u16 _y);
		kr::Bool mouseButtonReleased(MouseButton _button, kr::u16 _x, kr::u16 _y);
		kr::Bool mouseWheelScrolled(kr::u8 _amount);
		kr::Bool mouseMoved(kr::u16 _x, kr::u16 _y);
		kr::Bool textTyped(kr::u32 _character);

	};



	// =======================================================
	//  An input listener that listens for a key combination.
	// =======================================================
	class ComboListener : public InputListener {
	public:
		// A callback for when the key combination is pressed.
		// Takes a boolean that is true if the combination was pressed,
		// and false if the combination was released.
		using Callback = typename std::function<kr::Bool(kr::Bool)>;

	private:
		kr::Combo m_key;
		Callback m_callback;

		kr::Bool m_pressed = false;

	public:
		// Constructor.
		// _key : Key combo to listen for.
		// _callback : A callback that is called when the key combo is pressed or released.
		ComboListener(kr::Combo _key, const Callback &_callback)
			: m_key(_key), m_callback(_callback) {}

		kr::Bool keyPressed(Keycode _key, kr::Bool _ctrl, kr::Bool _alt, kr::Bool _shift) override;
		kr::Bool keyReleased(Keycode _key, kr::Bool _ctrl, kr::Bool _alt, kr::Bool _shift) override;
	};

}