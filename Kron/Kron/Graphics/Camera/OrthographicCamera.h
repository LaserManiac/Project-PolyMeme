#pragma once

#include "Core\Types.h"
#include "Graphics\Camera\Camera.h"

namespace kr {

	class OrthographicCamera : public Camera {
	
	public:
		OrthographicCamera() {};
		OrthographicCamera(float _width, float _height, float _near, float _far);

		void setToOrtho2D(kr::f32 _x, kr::f32 _y, kr::f32 _width, kr::f32 _height, kr::Bool yUp = true);

		void update() override;

	};

}
