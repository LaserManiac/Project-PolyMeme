#include "WorkerThread.h"

#include "Obsolete\Task.h"
#include "Core\String.h"
#include "Core\Exception.h"

namespace kr {

	WorkerThread::WorkerThread(ObjectProvider<Task>* _pTaskProvider) {
		if (!_pTaskProvider)
			throw Exception(kr::str("TaskProvider can not be NULL"));

		m_thread = std::thread([=]() {
			Task* pTask;
			do {
				pTask = _pTaskProvider->obtain();
				if (pTask) {
					(*pTask)();
					_pTaskProvider->release(pTask);
				}
			} while (pTask);
		});
	}

	WorkerThread::~WorkerThread() {
		if (m_thread.joinable())
			m_thread.join();
	}

}