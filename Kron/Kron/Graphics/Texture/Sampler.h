#pragma once

#include <GL\glew.h>
#include <SFML\OpenGL.hpp>

namespace kr {

	class Sampler {

	private:
		GLuint m_handle;

	public:
		Sampler(const Sampler &_other) = delete;
		Sampler& operator=(const Sampler &_other) = delete;

		Sampler();
		~Sampler();

		void bind(GLenum _unit) const;
		void unbind(GLenum _unit) const;

		GLuint getHandle() const;

		void setFiltering(GLenum _min, GLenum _mag);
		void setWrapping(GLenum _s, GLenum _t, GLenum _r);

	};

}