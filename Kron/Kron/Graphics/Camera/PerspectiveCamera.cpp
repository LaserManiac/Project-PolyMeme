#include "PerspectiveCamera.h"

kr::PerspectiveCamera::PerspectiveCamera(float _width, float _height, float _fov, float _near, float _far) 
	: kr::Camera(_width, _height, _near, _far), m_fov(_fov) {}

float kr::PerspectiveCamera::getFov() const {
	return m_fov;
}

void kr::PerspectiveCamera::setFov(float _fov) {
	m_fov = _fov;
}

void kr::PerspectiveCamera::update() {
	m_view.setToView(m_position, m_direction, m_up);
	m_projection.setToPerspective(m_viewportWidth, m_viewportHeight, m_fov, m_near, m_far);

	m_combined = m_view;
	gmtl::preMult(m_combined, m_projection);
}
