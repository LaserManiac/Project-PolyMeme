#pragma once

#include <functional>
#include <vector>

#include "Class.h"
#include "Types.h"
#include "Exception.h"

namespace kr {

	// ===========================================
	//  Forward declaration of EventSubscription.
	// ===========================================
	template<class... _Args>
	class EventSubscription;



	// ===========================================
	//  An event that callbacks can subscribe to.
	// ===========================================
	template<class... _Args>
	class Event : kr::NonCopyable {
	public:
		using Callback = typename std::function<void(_Args...)>;
		using Subscription = typename EventSubscription<_Args...>;

	private:
		std::vector<Callback*> m_callbacks;
		std::vector<Callback*> m_managedCallbacks;

	public:
		~Event() {
			m_callbacks.clear();
			for (Callback* callback : m_managedCallbacks)
				delete callback;
			m_managedCallbacks.clear();
		}

		// Add a callback to this event.
		// An Subscription object is returned.
		// _callback : A functor that can be wrapped in an std::function.
		template<class _Type>
		Subscription subscribe(const _Type &_callback) {
			Callback* pCallback = new Callback(_callback);
			m_callbacks.emplace_back(pCallback);
			return Subscription(this, pCallback);
		}

		// Call subscribe().
		template<class _Type>
		Subscription operator+(const _Type &_callback) {
			return subscribe(_callback);
		}

		// Call subscribe().
		template<class _Type>
		Subscription operator+=(const _Type &_callback) {
			return subscribe(_callback);
		}

		// Remove the callback from the list of subscriptions.
		// [WARNING] Intended for internal use!
		// To unsubscribe properly, use the Subscription object returned on subscribe().
		void unsubscribe(Callback* _pCallback) {
			auto iter = std::find(m_callbacks.begin(), m_callbacks.end(), _pCallback);
			if (iter != m_callbacks.end())
				m_callbacks.erase(iter);
		}

		// Add the callback to the list of managed callbacks,
		// it is deleted when the event is destroyed.
		// [WARNING] Intended for internal use!
		void lock(Callback* _pCallback) {
			m_managedCallbacks.emplace_back(_pCallback);
		}

		// Run all callbacks in this event with the provided arguments.
		void run(_Args... _args) {
			for (Callback* callback : m_callbacks)
				(*callback)(_args...);
		}

	};



	// ====================================
	//  A subscription to an event object.
	// ====================================
	template<class... _Args>
	class EventSubscription : kr::NonCopyable {
	private:
		using MyEvent = typename Event<_Args...>;
		using MyCallback = typename MyEvent::Callback;

	private:
		MyCallback* m_pCallback = NULL;
		MyEvent* m_pEvent = NULL;

	public:
		// Default constructor that creates an expired subscription.
		EventSubscription()
			: m_pEvent(NULL), m_pCallback(NULL) {}
		
		// [WARNING] Intended for internal use!
		// _pEvent : Event to which the callback is subscribed.
		// _pCallback : The callback which subscribed to the event.
		EventSubscription(MyEvent* _pEvent, MyCallback* _pCallback)
			: m_pEvent(_pEvent), m_pCallback(_pCallback) {}

		// Destructor. If this subscription is not expired, unsubscribe from the event.
		~EventSubscription() {
			if (!isExpired())
				unsubscribe();
		}

		// Move constructor.
		EventSubscription(EventSubscription &&_other) {
			m_pCallback = _other.m_pCallback;
			m_pEvent = _other.m_pEvent;
			
			_other.m_pCallback = NULL;
			_other.m_pEvent = NULL;
		}

		// Move assignment operator.
		// If this subscription is not expired, unsubscribe from the event.
		EventSubscription& operator=(EventSubscription &&_other) {
			if (!isExpired())
				unsubscribe();

			m_pCallback = _other.m_pCallback;
			m_pEvent = _other.m_pEvent;

			_other.m_pCallback = NULL;
			_other.m_pEvent = NULL;

			return *this;
		}

		MyEvent* getEvent() { return m_pEvent; }
		MyCallback* getCallback() { return m_pCallback; }
		kr::Bool isExpired() { return m_pEvent == NULL && m_pCallback == NULL; }

		// Remove the callback from the event.
		// The subscription expires after this.
		void unsubscribe() {
			if (isExpired())
				throw kr::InvalidStateException("This subscription has expired.");

			m_pEvent->unsubscribe(m_pCallback);
			delete m_pCallback;

			m_pEvent = NULL;
			m_pCallback = NULL;
		}

		// Transfer ownership of the callback to the event.
		// The subscription expires after this.
		void lock() {
			if (isExpired())
				throw kr::InvalidStateException("This subscription has expired.");

			m_pEvent->lock(m_pCallback);

			m_pEvent = NULL;
			m_pCallback = NULL;
		}

	};

}