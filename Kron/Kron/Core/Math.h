#pragma once

#include <GMTL\gmtl.h>

#include "Types.h"

namespace gmtl {

	using Vec2u = typename Vec<kr::u32, 2U>;
	using Vec3u = typename Vec<kr::u32, 3U>;
	using Vec4u = typename Vec<kr::u32, 4U>;

}
