#include "OrthographicCamera.h"

#include "Core\Math.h"

kr::OrthographicCamera::OrthographicCamera(float _width, float _height, float _near, float _far)
	: kr::Camera(_width, _height, _near, _far) {}

void kr::OrthographicCamera::setToOrtho2D(kr::f32 _x, kr::f32 _y, kr::f32 _width, kr::f32 _height, kr::Bool yUp) {
	m_viewportWidth = _width;
	m_viewportHeight = _height;
	m_position = gmtl::Point3f(_x + _width / 2, _y + _height / 2, 0);
	m_direction = gmtl::Vec3f(0, 0, 1);
	m_up = gmtl::Vec3f(0, (yUp ? 1.0f : -1.0f), 0);
	m_near = 0;
	m_far = 100;
	update();
}

void kr::OrthographicCamera::update() {
	m_view.setToView(m_position, m_direction, m_up);
	m_projection.setToOrthographic(m_viewportWidth, m_viewportHeight, m_near, m_far);

	m_combined = m_view;
	gmtl::preMult(m_combined, m_projection);
}
