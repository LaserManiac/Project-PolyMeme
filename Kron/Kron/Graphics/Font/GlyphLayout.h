#pragma once

#include <map>

#include <SFML\System.hpp>

namespace kr {

	class GlyphLayout {

	public:
		using Glyph = struct s_Glyph {
			const sf::Uint16 x;
			const sf::Uint16 y;
			const sf::Uint16 width;
			const sf::Uint16 height;
			const sf::Int16 offset_x;
			const sf::Int16 offset_y;
			const sf::Int16 advance_x;
			const sf::Uint8 page;

			s_Glyph(sf::Uint16 _x, sf::Uint16 _y, sf::Uint16 _width, sf::Uint16 _height, sf::Int16 _offset_x, sf::Int16 _offset_y, sf::Int16 _advance_x, sf::Uint8 _page)
				: x(_x),
				  y(_y), 
				  width(_width),
				  height(_height),
				  offset_x(_offset_x),
				  offset_y(_offset_y),
				  advance_x(_advance_x),
				  page(_page) {};
		};

	private:
		std::map<const sf::Uint16, const Glyph> m_glyphs;

	public:
		GlyphLayout() = delete;

		void addGlyph(sf::Uint16 _code, const Glyph _glyph);

		const Glyph& getGlyph(sf::Uint16 _code) const;

	};

}