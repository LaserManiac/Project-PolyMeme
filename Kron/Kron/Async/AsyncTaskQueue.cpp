#include "AsyncTaskQueue.h"

#include "Core\String.h"
#include "Core\Exception.h"

namespace kr {

	void AsyncTaskQueue::enqueue(Task* _pTask) {
		std::lock_guard<std::mutex> lock(m_mutex);
		if (m_closed)
			throw Exception(kr::str("AsyncTaskQueue is closed"));
		m_tasks.push(_pTask);
		if (m_waitingThreadCount > 0)
			m_conditionVariable.notify_one();
	}

	Task* AsyncTaskQueue::obtain() {
		std::unique_lock<std::mutex> lock(m_mutex);
		if (m_tasks.empty()) {
			if (m_closed)
				return NULL;
			m_waitingThreadCount++;
			m_conditionVariable.wait(lock);
			m_waitingThreadCount--;
			if (m_tasks.empty())
				return NULL;
		}
		Task* pTask = m_tasks.front();
		m_tasks.pop();
		return pTask;
	}

	void AsyncTaskQueue::release(Task* _pTask) {
		delete _pTask;
	}

	void AsyncTaskQueue::close() {
		std::lock_guard<std::mutex> lock(m_mutex);
		m_closed = true;
		m_conditionVariable.notify_all();
	}

}