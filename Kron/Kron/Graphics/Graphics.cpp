#include "Graphics.h"

#include "Core\Types.h"
#include "Core\Files.h"
#include "Core\Logging.h"
#include "Core\Exception.h"

namespace kr {

	kr::Bool Graphics::init() {
		GLenum glStatus = glewInit();
		return glStatus != GLEW_OK;
	}

	std::ostream& operator<<(std::ostream &_out, const GfxError &_error) {
		_out << "[OpenGL ERROR " << _error.m_error << ": " << GfxErrors::decodeError(_error.m_error) << "]";
		_out << " in file " << kr::FilePath(_error.m_file).filename();
		_out << " at line " << _error.m_line << ":\n";
		_out << _error.m_code << "\n";
		return _out;
	}

	void GfxErrors::defaultErrorHandler(const GfxError &_error) {
		kr::Logging::console(_error);
		throw _error;
	}

	void GfxErrors::setErrorHandler(ErrorHandler _handler) {
		s_errorHandler = _handler;
	}

	const kr::String GfxErrors::decodeError(GLenum _error) {
		switch (_error) {
		case GL_INVALID_ENUM:					return "GL_INVALID_ENUM";
		case GL_INVALID_VALUE:					return "GL_INVALID_VALUE";
		case GL_INVALID_OPERATION:				return "GL_INVALID_OPERATION";
		case GL_STACK_OVERFLOW:					return "GL_STACK_OVERFLOW";
		case GL_STACK_UNDERFLOW:				return "GL_STACK_UNDERFLOW";
		case GL_OUT_OF_MEMORY:					return "GL_OUT_OF_MEMORY";
		case GL_INVALID_FRAMEBUFFER_OPERATION:	return "GL_INVALID_FRAMEBUFFER_OPERATION";
		case GL_CONTEXT_LOST:					return "GL_CONTEXT_LOST";
		case GL_TABLE_TOO_LARGE:				return "GL_TABLE_TOO_LARGE";
		case GL_NO_ERROR:						return "GL_NO_ERROR";
		default:								return "GL_UNKNOWN_ERROR";
		}
	}

	void GfxErrors::clearErrors() {
		while (glGetError() != GL_NO_ERROR);
	}

	void GfxErrors::handleErrors(const kr::String &_file, kr::i32 _line, const kr::String &_code) {
		while (GLenum errorCode = glGetError()) {
			const GfxError gfxError(errorCode, _line, _file, _code);
			if (s_errorHandler != NULL)
				s_errorHandler(gfxError);
		}
	}

	GfxErrors::ErrorHandler GfxErrors::s_errorHandler = GfxErrors::defaultErrorHandler;

}
