#include "Sampler.h"

kr::Sampler::Sampler() {
	glGenSamplers(1, &m_handle);
}

kr::Sampler::~Sampler(){
	glDeleteSamplers(1, &m_handle);
}

void kr::Sampler::bind(GLenum _unit) const {
	glBindSampler(_unit, m_handle);
}

void kr::Sampler::unbind(GLenum _unit) const {
	glBindSampler(_unit, 0);
}

GLuint kr::Sampler::getHandle() const {
	return m_handle;
}

void kr::Sampler::setFiltering(GLenum _min, GLenum _mag) {
	glSamplerParameteri(m_handle, GL_TEXTURE_MIN_FILTER, _min);
	glSamplerParameteri(m_handle, GL_TEXTURE_MAG_FILTER, _mag);
}

void kr::Sampler::setWrapping(GLenum _s, GLenum _t, GLenum _r) {
	glSamplerParameteri(m_handle, GL_TEXTURE_WRAP_S, _s);
	glSamplerParameteri(m_handle, GL_TEXTURE_WRAP_T, _t);
	glSamplerParameteri(m_handle, GL_TEXTURE_WRAP_R, _r);
}
