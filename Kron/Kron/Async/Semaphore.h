#pragma once

#include <mutex>
#include <condition_variable>

#include <SFML\System\NonCopyable.hpp>

#include "Core\Exception.h"

namespace kr {

	class Semaphore : public sf::NonCopyable {
	private:
		std::mutex m_mutex;
		std::condition_variable m_condition;
		int m_counter;

	public:
		Semaphore(int _counter = 0);

		void obtain();
		void release();
		void wait();
		int counter();

	};

}