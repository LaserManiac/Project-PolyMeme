#pragma once

#include "Camera.h"

#include <GMTL\Point.h>
#include <GMTL\Vec.h>
#include <GMTL\VecOps.h>
#include <GMTL\Frustum.h>
#include <GMTL\FrustumOps.h>

#include "..\Projection.h"
#include "..\Transform.h"

namespace kr {

	class PerspectiveCamera : public Camera {

	private:
		float m_fov;

	public:
		PerspectiveCamera() {};
		PerspectiveCamera(float _width, float _height, float _fov, float _near, float _far);

		float getFov() const;
		void setFov(float _fov);

		void update() override;

	};

}
