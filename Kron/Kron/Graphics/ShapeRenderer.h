#pragma once

#include <vector>

#include "Core\Types.h"
#include "Graphics\Graphics.h"
#include "Graphics\Ogl\OglBuffer.h"
#include "Graphics\Ogl\OglVertexArray.h"
#include "Graphics\Ogl\OglShader.h"

#include "Core\Logging.h"

namespace kr {

	class ShapeRenderer {
	public:
		enum ShapeType {
			NONE,
			FILLED,
			LINE,
			POINT
		};

	private:
		std::vector<kr::f32> m_data;
		kr::OglArrayBuffer m_arrayBuffer;
		
		ShapeType m_shapeType = ShapeType::NONE;

		kr::Color m_color;
		gmtl::Matrix44f m_viewProjectionMatrix;
		kr::OglShaderProgram* m_pShader;

	public:
		void setViewProjectionMatrix(const gmtl::Matrix44f &_mat) { m_viewProjectionMatrix = _mat; }
		const gmtl::Matrix44f& getViewProjectionMatrix() const { return m_viewProjectionMatrix; }

		void setColor(const kr::Color &_color) { m_color = _color; }
		const kr::Color& getColor() const { return m_color; }

		void setShaderProgram(kr::OglShaderProgram* _pShader) { m_pShader = _pShader; }
		kr::OglShaderProgram* getShaderProgram() const { return m_pShader; }

		void begin(ShapeType _shapeType);
		void vertex(const gmtl::Vec3f &_position);
		void end();

	private:
		void flush();

	private:
		static const kr::u8 VERTEX_SIZE = 6U;
		static const kr::u8 VERTS_IN_TRIANGLE = 3U;
		static const kr::u8 VERTS_IN_LINE = 2U;

	};

}